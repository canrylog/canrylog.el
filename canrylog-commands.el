;;; canrylog-commands.el --- Outer interface for Canrylog -*- lexical-binding: t -*-

;;; Commentary:

;; NOTE: move with-file fragments to canrylog-file.el to support
;; multiple `canrylog-file's in the future

;;; Code:

(require 'transient)
(require 'dash)
(require 's)

(require 'canrylog-utils)
(require 'canrylog-file)

(require 'canrylog-vars)
(require 'canrylog-read)
(require 'canrylog-methods)
(require 'canrylog-db)

(transient-define-prefix canrylog-metadata-dispatch ()
  "Metadata related commands."
  ["Tags"
   ("t" "Add tag"                     canrylog-task-add-tag)
   ("d" "Delete a tag"                canrylog-task-delete-tag)]
  ["Metadata"
   ("s" "Set value for a key"         canrylog-task-set-metadata)
   ("x" "Delete a key"                canrylog-task-delete-metadata)])

(defun canrylog--rename-task (task-name new-name &optional force keep-children)
  "Rename TASK-NAME to NEW-NAME in the tracking file and the DB.

This is the internal function implementing both
`canrylog-rename-task' and `canrylog-rename-task-keep-children'.

If a task called NEW-NAME already exists, bail out unless FORCE
is non-nil: If FORCE is `prompt', prompt for confirmation; if it
is any other non-nil value, always forcefully rename.

If KEEP-CHILDREN is non-nil, rename children of TASK-NAME as well
to keep them under the same hierarchy. For example, when renaming
task a:b to c:b, a:b:x is also renamed to c:b:x if KEEP-CHILDREN
is non-nil. (Otherwise a:b:x will be left intact.)"
  (when (or (not (canrylog-task-exists? new-name))
            (not (equal task-name new-name))
            (and (eq force 'prompt)
                 (y-or-n-p
                  "New name collides with an existing task. Forcefully rename? "))
            force)
    (let ((task-regexp
           (format (if keep-children
                       ;; keep children = match a:b as well as a:b:c
                       (rx bol (*? any)
                           "  "
                           (group "%s")
                           ;; But do not match a:bc
                           (opt ":" (0+ any)) eol)
                     "^.*?  \\(%s\\)$")
                   (regexp-quote task-name))))
      (canrylog-with-file t
        (goto-char (point-min))
        (while (re-search-forward task-regexp nil t)
          (replace-match new-name t t nil 1)))
      (canrylog-db-rename-task task-name new-name keep-children))))

(defun canrylog-migrate-metadata (file)
  "Migrate FILE to the new database."
  (interactive "fPath to old tasks.json: ")
  (let-alist (let ((json-encoding-pretty-print t)
                   (json-object-type 'alist))
               (json-read-file file))
    ;; Insert tasks
    (dolist (task-pair .tasks)
      (let ((task (symbol-name (car task-pair)))
            (tags (alist-get 'tags (cdr task-pair))))
        (canrylog-db-execute "insert or replace into tasks values (?)" task)
        (--each (cl-coerce tags 'list)
          (canrylog-task-add-tag task it))
        (--each (cdr task-pair)
          (unless (eq (car it) 'tags)
            (let ((key (symbol-name (car it)))
                  (value (cdr it)))
              (canrylog-task-set-metadata task key value))))))))

(defun canrylog-migrate-timestamp ()
  "Migrate timestamps in `canrylog-file' to the new format.
The old format uses ISO 8601 timestamps, while the new format
uses unix timestamps which can be parsed much more quickly."
  (interactive)
  (canrylog-with-file t
    (canrylog-file-migrate-timestamp)))

;;;###autoload
(defun canrylog-rename-task-keep-children (task-name new-name)
  "Rename TASK-NAME to NEW-NAME in `canrylog-file', keeping its children."
  (interactive
   (let* ((old (canrylog-read-task "Rename task: "))
          (new (canrylog-read-new-task "To: " old)))
     (list old new)))
  (canrylog--rename-task task-name new-name 'prompt t))

;;;###autoload
(defun canrylog-rename-task (task-name new-name)
  "Rename TASK-NAME to NEW-NAME in `canrylog-file'."
  (interactive
   (let* ((old (canrylog-read-task "Rename task: "))
          (new (canrylog-read-new-task "To: " old)))
     (list old new)))
  (canrylog--rename-task task-name new-name 'prompt nil))

;;;###autoload
(defun canrylog-describe-moment (moment &optional message?)
  "Return the task that was active at MOMENT.

Echo the output if MESSAGE? is non-nil."
  (interactive
   (list (canrylog-read-time "Find task active at: ")
         t))
  (canrylog-with-file nil
    (canrylog-file-goto-moment moment)
    (let ((result (canrylog-event-task
                   (canrylog-file-event-at-point))))
      (if message? (message "%s" result) result))))

;;;###autoload
(defun canrylog-switch-task (task &optional moment)
  "Switch to TASK at MOMENT.

MOMENT defaults to the current time.

Do nothing if TASK is currently active.

Runs `canrylog-before-switch-task-hook' and
`canrylog-after-switch-task-hook' before and after switching.
`canrylog-after-save-hook' is also run after the tracking file is
modified."
  (interactive
   (list (canrylog-read-task "Switch to task: ")
         (if (transient-args 'canrylog-dispatch)
             (canrylog-read-time)
           (current-time))))
  (setq moment (canrylog-time-coerce
                (or moment (current-time))))
  (unless (equal (canrylog-current-task-name) task)
    (run-hooks 'canrylog-before-switch-task-hook)
    (canrylog-with-file t
      (goto-char (point-max))
      ;; fiddling with the final newline feels brittle; this seems more robust.
      (insert "\n" (format "%s  %s" (canrylog-serialize--moment moment) task))
      (canrylog-file-cleanup--remove-duplicate-newlines))
    (run-hooks 'canrylog-after-switch-task-hook)))

;;;###autoload
(defun canrylog-clock-out ()
  "Clock out by switching to `canrylog-downtime-task'."
  (interactive)
  (canrylog-switch-task canrylog-downtime-task))

;;;###autoload
(defun canrylog-visit ()
  "Visit `canrylog-file'."
  (interactive)
  (find-file (canrylog-file)))

(provide 'canrylog-commands)

;;; canrylog-commands.el ends here
