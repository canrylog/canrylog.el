;;; canrylog-file.el --- Functions to work within the tracking file  -*- lexical-binding: t; -*-
;;; Commentary:

;; Functions that are supposed to be run inside the tracking file.

;;; Code:

(require 'canrylog-utils)
(require 'canrylog-types)
(require 'canrylog-vars)
(require 'canrylog-cache)
(require 'transient)
(require 'subr-x)
(require 'thingatpt)
(require 'cl-lib)
(require 'dash)
(require 's)

(declare-function canrylog-read-time "canrylog-read")
(declare-function canrylog-read-task "canrylog-read")
(declare-function canrylog-get-events "canrylog-parse")

(defun canrylog-file-goto-moment (moment)
  "Move point to the line that was active for MOMENT."
  (setq moment (canrylog-time-coerce moment))
  ;; Binary search to find the last event that's earlier than MOMENT
  ;; Thank you Rosetta Code:
  ;; https://rosettacode.org/wiki/Binary_search#Emacs_Lisp
  (cl-do ((high (1- (count-lines
                     (point-min)
                     (point-max))))
          (low 0)
          mid)
      ((<= high low))
    (setq mid (floor (+ high low) 2))
    ;; Inlining this because `goto-line' is designed for interactive use
    (goto-char (point-min))
    (forward-line mid)
    (-if-let* ((event (canrylog-file-event-at-point))
               (event-moment (canrylog-event-moment event)))
        (cond ((canrylog-time-< moment event-moment)
               (setq high (1- (line-number-at-pos))))
              ((canrylog-time-< event-moment moment)
               (setq low (1+ (line-number-at-pos))))
              (t (cl-return nil)))
      ;; nudge `high' back if we can't grab the event
      (cl-decf high)))
  ;; Nudge forward to the right line
  (while (and (canrylog-time->= moment (canrylog-file-moment-at-point))
              (not (eobp)))
    (forward-line))
  ;; We've moved past the boundary to see the boundary. Move back.
  (forward-line -1))

(defun canrylog-file-migrate-timestamp-to-unix ()
  "Migrate ISO 8601 timestamps to unix timestamps in the current buffer."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward
            (rx bol (group (+ digit)) "  ")
            nil t)
      (replace-match
       (save-match-data
         (-> (match-string 1)
             canrylog-time-coerce--unix
             number-to-string))
       t t nil 1))))

(defun canrylog-file-migrate-timestamp-to-iso8601 ()
  "Migrate unix timestamps to ISO 8601 timestamps in the current buffer."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward
            (rx bol (group (+ digit)) "  ")
            nil t)
      (replace-match
       (save-match-data
         (-> (match-string 1)
             canrylog-time-coerce
             canrylog-iso8601))
       t t nil 1))))

(defun canrylog-file-event-at-point ()
  "Return event at point.
Return nil if the buffer is empty."
  (declare
   (gv-setter (lambda (new)
                `(let ((str (canrylog-serialize ,new)))
                   (save-excursion
                     (delete-region (line-beginning-position)
                                    (line-end-position))
                     (insert str))))))
  (cl-block nil
    (save-excursion
      (when (eobp)
        (condition-case _
            (backward-char)
          (error (cl-return nil))))
      (beginning-of-line)
      (search-forward "  " nil t)
      (canrylog-event
       :moment
       (buffer-substring-no-properties
        (line-beginning-position)
        (- (point)
           ;; the length of the separator "  "
           2))
       :task (buffer-substring-no-properties
              (point)
              (line-end-position))))))

(defun canrylog-file-moment-at-point ()
  "Return moment of event at point."
  (-some-> (canrylog-file-event-at-point)
    canrylog-event-moment))

(defun canrylog-file--bol-at-point (point)
  "Return the position of beginning of line at POINT."
  (save-excursion
    (goto-char point)
    (line-beginning-position)))

(defun canrylog-file--eol-at-point (point)
  "Return the position of end of line at POINT."
  (save-excursion
    (goto-char point)
    (line-end-position)))

(cl-defun canrylog-file-cleanup--sort (&optional (start (point-min)) (end (point-max)))
  "Sort events chronologically between START and END.

START and END default to beginning and end of buffer, respectively."
  (save-excursion
    (let* ((actual-start (canrylog-file--bol-at-point start))
           (actual-end (canrylog-file--eol-at-point end)))
      (sort-lines nil actual-start actual-end))))

(cl-defun canrylog-file-cleanup--remove-short-entries
    (&optional (start (point-min)) (end (point-max)))
  "Remove short entries.

- Delete event if next event happens within
  `canrylog-cleanup-short-intervals-threshold'.
- Delete event if previous event switches to the same task.

If START and END are provided, only do it between them."
  (interactive)
  (save-excursion
    (goto-char start)
    (let ((deleted-lines 0))
      (while (not (or (eobp)
                      (>= (point) end)))
        (when-let ((this (canrylog-file-event-at-point))
                   (next (save-excursion
                           (forward-line)
                           (unless (or (eobp)
                                       (>= (point) end))
                             (canrylog-file-event-at-point)))))
          ;; Delete event if next event happens within threshold
          ;; effectively deleting periods that are too short
          (when (< (- (canrylog-time-coerce--unix
                       (canrylog-event-moment next))
                      (canrylog-time-coerce--unix
                       (canrylog-event-moment this)))
                   canrylog-cleanup-short-intervals-threshold)
            (cl-incf deleted-lines)
            (canrylog--delete-line)))
        ;; Need to update values after above edit
        (when-let ((this (canrylog-file-event-at-point))
                   (prev (save-excursion
                           (forward-line -1)
                           (unless (or (bobp)
                                       (<= (point) start))
                             (canrylog-file-event-at-point)))))
          ;; Delete event if previous event switches to the same task
          (when (equal (canrylog-event-task this) (canrylog-event-task prev))
            (cl-incf deleted-lines)
            (canrylog--delete-line)))
        (forward-line))
      deleted-lines)))

(defun canrylog-file-cleanup--remove-duplicate-newlines ()
  "Remove duplicate newlines."
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "\n\n+" nil t)
      (replace-match "\n"))))

(defun canrylog-file-cleanup--remove-invalid-lines ()
  "Remove lines that are not valid events."
  (save-excursion
    (goto-char (point-min))
    (while (not (eobp))
      (if (s-contains? "  " (thing-at-point 'line t))
          (forward-line)
        (canrylog--delete-line)))))

(defun canrylog-file-cleanup (start end &optional sort?)
  "Cleanup `canrylog-file'.

1. Remove all invalid lines.
2. (When SORT? is non-nil) Sort lines chronologically between
   START and END.
3. Remove all duplicate newlines.
4. Remove all duplicate lines.
5. Clean up short intervals between START and END.

Interactively:
- When region is active, use its boundries as START and END.
- With one prefix argument, also sort between START and END.
- With two prefix arguments, clean up and sort the entire file.
- Otherwise only clean up lines between point and point - 50000,
  without sorting."
  (interactive
   (cond ((region-active-p)
          (list (region-beginning)
                (region-end)
                t))
         ((equal '(4) current-prefix-arg)
          (list (- (point) 50000) (point-max) t))
         ((equal '(16) current-prefix-arg)
          (list (point-min) (point-max) t))
         (t
          (list (- (point) 50000)
                (point-max)
                t))))
  (canrylog::loading "Removing invalid lines..."
    (canrylog-file-cleanup--remove-invalid-lines))
  (when sort?
    (canrylog::loading "Sorting..."
      (let ((pt (point)))
        (canrylog-file-cleanup--sort start end)
        (goto-char pt))))
  (canrylog::loading "Removing duplicate newlines..."
    (canrylog-file-cleanup--remove-duplicate-newlines))
  (canrylog::loading "Removing duplicate lines..."
    (delete-duplicate-lines start end))
  (when canrylog-cleanup-delete-short-intervals
    (canrylog::loading "Removing short entries..."
      (save-excursion
        ;; init to 1 (or any other >0 number) here
        ;; just makes it do it the first time
        (let ((deleted-lines 1)
              (pass 1))
          (while (> deleted-lines 0)
            (canrylog::message "Removing short entries... pass %s" pass)
            (goto-char start)
            (setq deleted-lines
                  (canrylog-file-cleanup--remove-short-entries
                   (line-beginning-position) end))
            (cl-incf pass))))))
  ;; Clear echo area
  (message ""))

(defun canrylog-file-do-merge ()
  "Handle merge conflicts, preserving every event and making sure to sort them."
  (interactive)
  (save-excursion
    (goto-char (point-max))
    (while (re-search-backward "^<<<" nil t)
      (let (start end)
        (prog1 "handling merge lines"
          (canrylog--delete-line)
          (setq start (point))
          (when (re-search-forward "^|||" nil t)
            (canrylog--delete-line))
          (when (re-search-forward "^===" nil t)
            (canrylog--delete-line))
          (when (re-search-forward "^>>>" nil t)
            (canrylog--delete-line))
          (setq end (point)))
        (canrylog-file-cleanup--sort start end)))))

(defun canrylog-file-change-time (time)
  "Change the time of event under point to TIME."
  (interactive
   ;; read requires parse, which requires this module
   ;; so require it inside here
   (progn
     (require 'canrylog-read)
     (list (canrylog-read-time "Change this event's moment to: "))))
  (unless (eq major-mode 'canrylog-file-mode)
    (user-error "%s" "Must be run in a canrylog file buffer"))
  (require 'canrylog-parse)
  (setq time (floor (canrylog-time-coerce--unix time)))
  (unless (canrylog-time-< (cadr (canrylog-get-events))
                           time)
    (user-error "Cannot change time to be past the previous event"))
  (let ((event (canrylog-file-event-at-point)))
    (setf (canrylog-file-event-at-point)
          (canrylog-event
           :moment time
           :task (canrylog-event-task event)))))

(defun canrylog-file-change-task (task)
  "Change the task of event under point to TASK."
  (interactive
   (progn
     (require 'canrylog-read)
     (list (canrylog-read-task "Change this event's task to: "))))
  (unless (eq major-mode 'canrylog-file-mode)
    (user-error "%s" "Must be run in a canrylog file buffer"))
  (let ((event (canrylog-file-event-at-point)))
    (setf (canrylog-file-event-at-point)
          (canrylog-event
           :moment (canrylog-event-moment event)
           :task task))))

(defvar canrylog-file-font-lock-keywords
  '(("^\\(.*?\\)  " . (1 font-lock-constant-face))
    (":" . font-lock-keyword-face))
  "Keywords to highlight in Canrylog File mode.")

(defvar canrylog-file-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c C-c") #'canrylog-file-cleanup)
    (define-key map (kbd "C-c C-e") #'canrylog-file-change-task)
    (define-key map (kbd "C-c C-t") #'canrylog-file-change-time)
    map)
  "Keymap for `canrylog-file-mode'.")

;;;###autoload
(define-derived-mode canrylog-file-mode fundamental-mode "Canrylog-File"
  "Major mode for editing Canrylog files.

\\{canrylog-file-mode-map}"
  (setq font-lock-defaults '(canrylog-file-font-lock-keywords))
  (add-hook 'after-save-hook #'canrylog-cache-invalidate nil t)
  (add-hook 'after-revert-hook #'canrylog-cache-invalidate nil t))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.canrylog\\'" . canrylog-file-mode))

(provide 'canrylog-file)

;;; canrylog-file.el ends here
