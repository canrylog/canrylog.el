;;; canrylog-vars.el --- Variables and user options  -*- lexical-binding: t; -*-
;;; Commentary:

;; Variables and user options for all of Canrylog.

;;; Code:

(require 'dash)

(defvar-local canrylog-view-state nil
  "Buffer-local variable for storing the state of a view.")
;; Use a macro to simplify setf
(defmacro canrylog--vget (key)
  "Return the value for KEY in `canrylog-view-state'."
  `(alist-get ',key canrylog-view-state))
(defmacro canrylog--vset (key &optional new-value)
  "Set the value for KEY in `canrylog-view-state' to NEW-VALUE.

If NEW-VALUE is nil, take the value from the variable value of
KEY. For example, these are equivalent:

  (canrylog--vset foo \"abc\")
  (setf (alist-get \\='foo canrylog-view-state) \"abc\")

  (canrylog--vset bar bar)
  (setf (alist-get \\='bar canrylog-view-state) bar)

  (canrylog--vset bar)
  (setf (alist-get \\='bar canrylog-view-state) bar)"
  (declare (indent 1))
  `(setf (alist-get ',key canrylog-view-state) ,(or new-value key)))
(defmacro canrylog--vlet (keys &rest body)
  "Bind the variables in KEYS to keys in `canrylog-view-state' then run BODY."
  (declare (indent 1))
  `(let ,(--map
          `(,it (alist-get ',it canrylog-view-state))
          keys)
     ,@body))

(define-button-type 'canrylog
  'follow-link t
  'face 'canrylog-button)

(define-button-type 'canrylog-item
  'follow-link t
  'face 'canrylog-item)

(defconst canrylog--base-dir (or (-some-> load-file-name
                                   file-truename
                                   file-name-directory)
                                 ;; If I'm eval-buffer'ing this buffer
                                 default-directory)
  "The directory where Canrylog is loaded from.")

(defconst canrylog-iso8601-fast-regexp
  ;; Slightly faster using (digit digit) instead of (= 2 digit)
  (rx (group digit digit digit digit) "-"
      (group digit digit) "-"
      (group digit digit)
      "T"
      (group digit digit) ":"
      (group digit digit) ":"
      (group digit digit)
      (or (group "Z")
          (seq (or "+" "-")
               (group digit digit)
               (opt ":")
               (group digit digit))))
  "A regexp to match ISO 8601 timestamps, sacrificing correctness for speed.")

;;;; User option
(defgroup canrylog nil
  "Track time."
  :group 'applications
  :prefix "canrylog-")

(defcustom canrylog-repo (expand-file-name "~/.canrylog")
  "The path to the repository."
  :group 'canrylog
  :type 'directory)
(defun canrylog-file ()
  "Return the path to the tracking file."
  (expand-file-name "tracking.canrylog" canrylog-repo))
(defun canrylog-db-file ()
  "Return the path to the DB file."
  (expand-file-name "canrylog.db" canrylog-repo))

(defcustom canrylog-timestamp-format 'iso8601
  "How timestamps should be stored in the tracking file.

`iso8601': timestamps are stored like \"2000-01-02T11:12:13+0900\".
  More readable but slower to parse.
`unix': timestamps are stored as unix timestamps.
  Faster to parse but less readable."
  :group 'canrylog
  :type '(choice (const :tag "ISO 8601" iso8601)
                 (const :tag "Unix timestamp" unix)))

(defcustom canrylog-cleanup-delete-short-intervals t
  "Should short intervals be cleaned up by `canrylog-file-cleanup'?

When this is nil, you can still run
`canrylog-file-cleanup--remove-short-entries' to delete short intervals."
  :group 'canrylog
  :type 'boolean)

(defcustom canrylog-cleanup-short-intervals-threshold (* 1 60)
  "Clean up intervals shorter than this many seconds."
  :group 'canrylog
  :type 'integer)

(defcustom canrylog-after-save-hook nil
  "Hook run after Canrylog saves modifications into the tracking file."
  :group 'canrylog
  :type 'hook)

(defcustom canrylog-before-save-hook nil
  "Hook run after writing changes but before saving them to the tracking file."
  :group 'canrylog
  :type 'hook)

(defcustom canrylog-before-switch-task-hook nil
  "Hook run before switching tasks."
  :group 'canrylog
  :type 'hook)

(defcustom canrylog-after-switch-task-hook nil
  "Hook run after switching tasks."
  :group 'canrylog
  :type 'hook)

(defcustom canrylog-downtime-task "Downtime"
  "The task that represents being clocked out."
  :group 'canrylog
  :type 'string)

(defcustom canrylog-time-format "%H:%M"
  "Display time in this format (see `format-time-string' for syntax)."
  :group 'canrylog
  :type 'string)

(defcustom canrylog-datetime-format "%FT%T%z"
  "Display datetime in this format (see `format-time-string' for syntax)."
  :group 'canrylog
  :type 'string)

(defcustom canrylog-sort-tasks-by-last-active t
  "Should tasks be sorted by when they were last active in completion prompts?

This makes listing task names slower."
  :group 'canrylog
  :type 'boolean)

;;;; Faces
(defgroup canrylog-ui-faces nil
  "Faces for use in the Canrylog UI."
  :group 'canrylog)

(defface canrylog-sans
  `((t (:font "Sans" :height 1.0)))
  "Sans serif."
  :group 'canrylog-ui-faces)

(defface canrylog-main-title
  `((t (:inherit canrylog-sans :weight bold :height 2.6)))
  "Main title."
  :group 'canrylog-ui-faces)

(defface canrylog-title
  '((t (:inherit canrylog-sans :height 1.4 :weight bold)))
  "Title face."
  :group 'canrylog-ui-faces)

(defface canrylog-heading
  '((t (:inherit canrylog-sans :height 1.0 :weight bold)))
  "Heading face."
  :group 'canrylog-ui-faces)

(defface canrylog-duration
  '((t (:inherit italic)))
  "Face for a duration."
  :group 'canrylog-ui-faces)

(defface canrylog-time
  '((t (:inherit shadow)))
  "Face for a moment in time."
  :group 'canrylog-ui-faces)

(defgroup canrylog-button-faces nil
  "Faces for buttons used in Canrylog."
  :group 'canrylog)

(defface canrylog-button
  `((t (
        :inherit (link canrylog-sans)
        :weight normal
        :height 1.0
        :underline t)))
  "Canrylog buttons."
  :group 'canrylog-button-faces)

(defface canrylog-active-item
  '((t (:inherit link :underline nil)))
  "Canrylog active items."
  :group 'canrylog-button-faces)

(defface canrylog-item
  '((t (:inherit link :underline nil :weight normal)))
  "Face for Canrylog items that can be clicked."
  :group 'canrylog-button-faces)

(defface canrylog-dangerous
  '((t (:inherit error)))
  "Face for dangerous actions."
  :group 'canrylog-button-faces)

(provide 'canrylog-vars)

;;; canrylog-vars.el ends here
