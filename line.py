#!/usr/bin/env python3
import matplotlib.pyplot as plt
import json
import sys

data = json.load(sys.stdin)

fig, ax = plt.subplots()
fig.set_size_inches(4, 4)
ax.plot(data)
fig.savefig(sys.stdout.buffer, dpi=100, transparent=True)
