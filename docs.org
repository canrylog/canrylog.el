#+title: Canrylog

* Introduction

Canrylog is a personal time tracking application. It has a few principles:

- Manual tracking
- No multitasking — only one task is active at a time, as humans are bad at multitasking (CITE)
- Time is tracked by logging task-switching /events/
  - The clocked out state is represented by switching to the task “Downtime”
  - Canrylog does not need to be running for a task to continue to be active
  - Tasks do not have to be declared beforehand

* Installation

Canrylog is not yet on MELPA. It can be installed directly from source:

With straight:

#+begin_src emacs-lisp
(straight-use-package
 '(canrylog :repo "canrylog/canrylog.el" :host gitlab))
#+end_src

* Getting started

Tracking data will be saved into the directory defined by =canrylog-repo=, which defaults to =~/.canrylog=.

The main entry point is =canrylog-dispatch=. This pops up a list of commands to choose from. Press =s= to switch to a new task.

Run =canrylog-dispatch= =s= again to switch to another task. To clock out, run =canrylog-dispatch= =o= — the clocked out state is represented by switching to a Downtime task.

=canrylog-dispatch= provides entry to various reports that can be done on the tracking data.

* Stability

I personally recommend putting =canrylog-repo= under version control to minimize the risk of losing data.

While the current feature set has minimal risk of data loss (the only modifications done are adding new events and local editing commands where Emacs's undo history is available), I plan to integrate synchronization into Canrylog — automatically merging changes can be finicky.

* Concepts
** TODO Goals
** TODO Tags
** Buffer management

There will be two navigation styles, =multiple= and =single=.


Like Helpful, each page has its own buffer.

The number of buffers is controlled by =canrylog-max-buffers=. When a new buffer is created, if the number of buffers would exceed =canrylog-max-buffers=, then the oldest buffer would be closed.

Canrylog buffers can be killed in one go with the command =canrylog-kill-buffers=.

* Reference
** TODO Commands
*** canrylog-kill-buffers

Kill all buffers created by Canrylog. 

** TODO User options
*** canrylog-max-buffers

The number of Canrylog buffers to keep. If =nil=, then never clean up old Canrylog buffers.

When a new Canrylog buffer is created, if the total number of Canrylog buffers would exceed this number, then old buffers would be killed.

This is like how Helpful's buffer management works.

** TODO Internal functions
