;;; canrylog-parse.el --- Parsing canrylog-file  -*- lexical-binding: t; -*-
;;; Commentary:

;; Parse the file -> save into a variable
;; Public functions:
;; get-tasks NAME ...: return list of parsed tasks matching NAME ...
;; get-task NAME: return a parsed task matching NAME
;; get-current-task: return current task
;; list-task-names: return list of task names
;; list-events: return list of events

;; These are here because they need parsing:
;; get-task-parent TASK: return parent of TASK in parsed tasks
;; get-task-children TASK: return children of TASK in parsed tasks

;;; Code:

(require 's)
(require 'subr-x)
(require 'dash)

(require 'canrylog-cache)
(require 'canrylog-file)
(require 'canrylog-types)
(require 'canrylog-utils)

(defun canrylog-parse-events-interval (interval)
  "Return events within INTERVAL in current buffer.

Results are returned from oldest to newest."
  (goto-char (point-max))
  (let (seen-first result this)
    (catch 'break
      ;; If INTERVAL is fully in the future of the latest event, that
      ;; means no event could be in it. Return nil instead of
      ;; iterating through the entire file.
      (when (canrylog-time-< (canrylog-file-event-at-point)
                             (oref interval start))
        (throw 'break nil))
      (while (and (not (bobp))
                  (setq this (canrylog-file-event-at-point)))
        ;; We're conceptually iterating through a list
        ;; like this:
        ;;
        ;;    | starting point
        ;;    v
        ;;   [n n n n n n y y y y y y n n n n ...]
        ;;
        ;; Before we enter the y block (tracked with
        ;; `seen-first' = nil), if this block does not
        ;; match, we simply skip to the next one. As we
        ;; enter the y block, we set `seen-first' to t,
        ;; now if we see another item that doesn't match
        ;; we exit.
        ;;
        ;; Note that we're going from point-max up.
        (cond
         ((canrylog-intersecting? this interval)
          (unless seen-first
            (setq seen-first t))
          (cl-pushnew this result :test #'equal)
          (forward-line -1))
         ((not seen-first)
          (forward-line -1))
         (t
          ;; We collect one event after the block ends, as the last
          ;; interval spills into the previous day.
          (cl-pushnew (canrylog-event :moment (-> (canrylog-event-moment this)
                                                  canrylog-time-coerce
                                                  (canrylog-time-clamp interval))
                                      :task (canrylog-event-task this))
                      result
                      :test #'equal)
          (throw 'break nil)))))
    result))

(defun canrylog-parse-task-names ()
  "Get names of all tasks in the tracking file."
  (let ((raw-names (cond (canrylog-sort-tasks-by-last-active
                          (canrylog-with-file nil
                            (goto-char (point-max))
                            (skip-chars-backward "\r\n")
                            (let (ret event)
                              (catch 'break
                                (while t
                                  (setq event (canrylog-file-event-at-point))
                                  (unless event
                                    (throw 'break nil))
                                  (push (canrylog-event-task event) ret)
                                  (when (/= 0 (forward-line -1))
                                    (throw 'break nil))))
                              (-uniq ret))))
                         (t (-uniq (mapcar #'canrylog-event-task (canrylog-get-events)))))))
    (let (result)
      (--each raw-names
        (setq result
              (nconc
               result
               (nconc
                (canrylog::task-name:ancestors it)
                (list it)))))
      (nreverse (-uniq result)))))

(defun canrylog-get-task-names ()
  "Get names of all tasks in the tracking file.

This utilizes `canrylog-cache'."
  (canrylog-cache 'task-names
    #'canrylog-parse-task-names))

;; faster than (s-split "\n" (buffer-string)) approach
;; or using regexps to match the line directly
(defun canrylog-parse-events ()
  "Return events in the tracking file, most recent first."
  (let (result)
    (canrylog-with-file nil
      (goto-char (point-min))
      (while (not (eobp))
        (push (canrylog-file-event-at-point) result)
        (forward-line)))
    result))

;; try (canrylog-get-events)
(defun canrylog-get-events ()
  "Get events from the tracking file.

This utilizes `canrylog-cache'."
  (canrylog-cache 'events
    #'canrylog-parse-events))

(cl-defun canrylog-cached-task-info (&key info name descendants range)
  "Like `canrylog-parse-task-info', but cached.
See `canrylog-parse-task-info' for INFO, NAME, DESCENDANTS, and RANGE."
  (setq name (ensure-list name))
  (canrylog-cache-table 'task-info
    (lambda ()
      (canrylog-parse-task-info
       :info info
       :name name
       :range range
       :descendants descendants))
    ;; If the task is current, its durations and intervals are still changing.
    ;; In that case, pass nil as ID so that the cache is bypassed.
    (unless (and name (member (canrylog-current-task-name) name))
      (list info name range descendants))))

(defun canrylog-cached-task-descendants-duration (task &optional range)
  "Like `canrylog-parse-task-descendants-duration', but cached.
TASK is the task name, RANGE limits the reporting. See
`canrylog-parse-task-descendants-duration' for details."
  (canrylog-cache-table 'task-descendants-duration
    (lambda ()
      (canrylog-parse-task-descendants-duration task range))
    (unless (or (equal (canrylog-current-task-name) task)
                ;; If current task is a descendant of `task', then the durations
                ;; of `task' would still be changing. So we do not want to cache
                ;; in this case.
                (canrylog::task-name:descendant-of-p
                 (canrylog-current-task-name)
                 task))
      (list task range))))

(cl-defun canrylog-parse-for-each-event-pair (task &key descendants range fn)
  "Run FN for each pair of events for TASK.

FN is called with three arguments: the name of the starting
event, the starting time, and the ending time.

- TASK: match events that switch to the task with this exact
  name. This can also be a list, matching multiple task names
  exactly. If nil, match any task. Must not be nil if DESCENDANTS
  is specified.

- DESCENDANTS: If nil, only match tasks that is called TASK
  exactly. If non-nil, also include its descendants (eg.
  \"Abc:Def\" for \"Abc\").

- RANGE: optional. If provided, should be a `canrylog-interval'.
  Only return intervals or duration within RANGE. Intervals are
  capped to be within RANGE as well."
  (declare (indent 1))
  (when (and descendants (not task))
    (error "TASK must be provided"))
  (when range
    (setq range
          (canrylog-interval
           :start (canrylog-time-coerce--unix (oref range start))
           :end (canrylog-time-coerce--unix (oref range end)))))
  (canrylog-with-file nil
    (save-restriction
      (when (canrylog-interval-p range)
        (let ((point-start
               (progn
                 (canrylog-file-goto-moment (oref range start))
                 (point)))
              (point-end
               (progn
                 (canrylog-file-goto-moment (oref range end))
                 ;; end on next event, if any
                 (forward-line 2)
                 (point))))
          (narrow-to-region point-start point-end)))
      (goto-char (point-min))
      (let ((main-regexp
             (rx-to-string
              `(seq (group (*? any)) "  "
                (group ,(if task
                            `(regexp ,(regexp-opt (ensure-list task)))
                          '(*? (not "\n")))
                       ,@(when descendants
                           '((opt ":")
                             (*? (not "\n")))))
                (or (seq "\n" (group (*? any)) "  ")
                    eol)))))
        (while (re-search-forward main-regexp nil t)
          ;; Avoid calling `match-string' twice.
          ;;
          ;; Previously the (when second ...) check below just accessed
          ;; (match-string 2) directly, but while adding back support for
          ;; iso 8601 timestamps in the file, there was a bizzare error where
          ;; `(match-string 2)' would error out with
          ;; (args-out-of-range #<buffer iso8601.canrylog> 0 1) but only on
          ;; the second call.
          (let ((first (match-string 1))
                (this-task (match-string-no-properties 2))
                (second (match-string 3))
                start end)
            ;; Avoids skipping two lines at once, but only when we have
            ;; matched the second line.
            (when second
              (beginning-of-line))
            (setq start (canrylog-time-coerce first)
                  end (canrylog-time-coerce second))
            (funcall fn this-task start end)))))))

(defun canrylog-parse-task-descendants-duration (task &optional range)
  "Return durations for each descendant of TASK.
If a descendant has descendants, those are counted multiple times for
each ancestor.
If RANGE is provided, cap durations within RANGE.

The return value is a hash table mapping task names to durations in
seconds.

This ensures ancestors of each task exists, unlike
`canrylog-parse-task-info'\\='s distribution mode."
  (let ((table (make-hash-table :test 'equal)))
    (canrylog-parse-for-each-event-pair task
      :descendants t
      :range range
      :fn (lambda (this-task start end)
            (--each (cons this-task (canrylog::task-name:ancestors this-task))
              (cl-incf (gethash it table 0)
                       ;; TODO: do we need to clamp it here?
                       (- (canrylog-time-clamp end range)
                          (canrylog-time-clamp start range))))))
    table))

(cl-defun canrylog-parse-task-info (&key info name descendants range)
  "Return info for task NAME.

Arguments:

- INFO: required, options:
  - `duration': return total duration of matched tasks
  - `intervals': return all intervals of matched tasks in a flat
    list
  - `distribution': return duration for each task in the
    format ((TASK . DURATION) ...)

- NAME: match events that switch to the task with this exact
  name. This can also be a list, matching multiple task names
  exactly. If nil, match any task. Must not be nil if DESCENDANTS
  is specified.

- DESCENDANTS: only applicable when using NAME. If nil, only match
  tasks that is called NAME exactly. If non-nil, also include its
  descendants (eg. \"Abc:Def\" for \"Abc\").

- RANGE: optional. If provided, should be a `canrylog-interval'.
  Only return intervals or duration within RANGE. Intervals are
  capped to be within RANGE as well."
  (let ((ret (pcase info
               (`duration 0)
               (`distribution nil)
               (_ nil))))
    (canrylog-parse-for-each-event-pair name
      :descendants descendants
      :range range
      :fn (lambda (task start end)
            (pcase info
              (`distribution
               (cl-incf
                (alist-get task ret 0 nil #'equal)
                ;; TODO: why is this capped again?
                (- (canrylog-time-clamp end range)
                   (canrylog-time-clamp start range))))
              (`duration
               (cl-incf
                ret
                (- (canrylog-time-clamp end range)
                   (canrylog-time-clamp start range))))
              (`intervals
               (push
                (canrylog-interval
                 :task task
                 :start (canrylog-time-clamp start range)
                 :end (canrylog-time-clamp end range))
                ret)))))
    ret))

(defun canrylog-current-task-name ()
  "Return the name of the currently active task."
  (canrylog-cache 'current-task-name
    (lambda ()
      (canrylog-with-file nil
        (goto-char (1- (point-max)))
        (when-let (event (canrylog-file-event-at-point))
          (canrylog-event-task event))))))

(provide 'canrylog-parse)

;;; canrylog-parse.el ends here
