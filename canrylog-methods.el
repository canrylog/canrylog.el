;;; canrylog-methods.el --- methods that depend on canrylog-types dependents  -*- lexical-binding: t; -*-
;;; Commentary:
;; `canrylog-task-duration-plus-children' requires `canrylog-parse', which
;; requires `canrylog-types'. There are two solutions: either
;; `require' `canrylog-parse' in the body of
;; `canrylog-task-duration-plus-children', which is ugly and causes the byte
;; compiler to think the `canrylog-parse' functions are not defined;
;; or put those functions in another file. This is that other file.
;;; Code:

(require 'cl-lib)
(require 'dash)
(require 'canrylog-parse)
(require 'canrylog-db)
(require 'canrylog-types)

(require 'canrylog-ifn)

(require 'canrylog-vars)

(defun canrylog-day-intervals-between-moments (start end)
  "Return days (as a list of intervals) between START and END.
This assumes START comes before END."
  (let* ((start-day (canrylog-interval-from-name 'day-containing start))
         (end-day (canrylog-interval-from-name 'day-containing end))
         ;; eg. 00:00 of the day of this event
         ;; We want numbers to be able to easily jump forward.
         (start-day-start (canrylog-time-coerce--unix (oref start-day start)))
         (end-day-start (canrylog-time-coerce--unix (oref end-day start)))
         days)
    ;; Get each day between start and end. This is necessary instead
    ;; of simple clamping with the start and end days, as that would
    ;; assume a task only runs for less than 24 hours.
    (let ((pos start-day-start))
      (while (<= pos end-day-start)
        ;; We don't need to do pushnew because Emacs doesn't support leap
        ;; seconds anyways
        (push (canrylog-interval-from-name 'day-containing pos)
              days)
        (cl-incf pos 86400)))
    days))

(defun canrylog-week-intervals-between-moments (start end)
  "Return weeks (as a list of intervals) between START and END.
This assumes START comes before END."
  (let* ((start-week (canrylog-interval-from-name 'week-containing start))
         (end-week (canrylog-interval-from-name 'week-containing end))
         (start-week-start (canrylog-time-coerce--unix (oref start-week start)))
         (end-week-start (canrylog-time-coerce--unix (oref end-week start)))
         weeks)
    (let ((pos start-week-start))
      (while (<= pos end-week-start)
        (push (canrylog-interval-from-name 'week-containing pos)
              weeks)
        (cl-incf pos (* 86400 7))))
    weeks))

(defun canrylog--events-distribution (events)
  "Calculate distribution in EVENTS.

Remember to bind `canrylog-interval-duration--bound' to specify
when the end of the final interval should actually be.

The return value is in the form ((TASK . DURATION) ...).

This assumes EVENTS is sorted by `canrylog-time-<'."
  (let ((events (copy-sequence events))
        (distribution))
    (while events
      (let* ((this (car events))
             (task (oref this task))
             (next (cadr events)))
        ;; Edge case: for the last event, the "next" is the current time
        (unless next
          (setq next (canrylog-event :moment (float-time))))
        (cl-incf (alist-get task distribution 0 nil #'equal)
                 (- (canrylog-time-coerce--unix (oref next moment))
                    (canrylog-time-coerce--unix (oref this moment)))))
      (pop events))
    (--sort (> (cdr it) (cdr other))
            distribution)))

(defun canrylog-task-children (task)
  "Return children (really, descendants) of TASK."
  (setq task (canrylog::ensure-task-name task))
  (--filter (canrylog::task-name:descendant-of-p it task)
            (canrylog-get-task-names)))

(defun canrylog-task-intervals-in-intervals (task intervals)
  "Return intervals that belong to TASK within INTERVALS."
  (--filter (equal (oref it task) task) intervals))

(defun canrylog-task-first-active (task)
  "Return the first active moment of TASK.
TASK can also be a list of tasks, in which case return the
earliest of them."
  (let* ((tasks (ensure-list task))
         (names (mapcar #'canrylog::ensure-task-name tasks)))
    (canrylog-time-coerce
     (canrylog-event-moment
      (--first
       (let ((n (canrylog-event-task it)))
         (--any?
          (or (equal it n)
              (canrylog::task-name:parent-of-p it n))
          names))
       (reverse (canrylog-get-events)))))))

(defun canrylog-task-last-active (task &optional no-end-is-now descendants)
  "Return last active moment of TASK.

If TASK is still active:
- return nil normally;
- return non-nil if NO-END-IS-NOW is non-nil.

  This allows the caller to synchronize what \"now\" means across
  multiple invocations.

If TASK is never active, return `never'.

If DESCENDANTS is non-nil, also consider descendants."
  (setq task (canrylog::ensure-task-name task))
  (canrylog-with-file nil
    (goto-char (point-max))
    ;; Search for TASK and move to the next line
    (and (re-search-backward
          (canrylog-regexp-task task descendants)
          nil t)
         (forward-line))
    (let ((event-at-point (canrylog-file-event-at-point)))
      (if (eq (point) (point-max))
          ;; If we haven't moved, that means we didn't find anything or the task is
          ;; currently active.
          (if (or (equal task
                         (canrylog-event-task event-at-point))
                  (and descendants
                       (canrylog::task-name:descendant-of-p
                        (canrylog-event-task event-at-point)
                        task)))
              ;; The task is currently active
              (and no-end-is-now (current-time))
            ;; We didn't find anything
            'never)
        ;; Otherwise we have moved and the task isn't active
        (canrylog-time-coerce
         (canrylog-event-moment event-at-point))))))

(defun canrylog-task-last-active-relative (task &optional descendants)
  "Return how long ago TASK was last active, in seconds.
If DESCENDANTS is non-nil, also consider TASK\\='s descendants."
  (cl-block nil
    (let ((last-active (canrylog-task-last-active task nil descendants)))
      (unless last-active
        (cl-return 'now))
      (when (eq last-active 'never)
        (cl-return 'never))
      (abs
       (- (canrylog-time-coerce--unix (float-time))
          (canrylog-time-coerce--unix last-active))))))

(defun canrylog-task-duration (task &optional range)
  "Return duration of TASK, in seconds.
If RANGE is a `canrylog-interval', only take into account the
amount of time TASK has been active with RANGE."
  (canrylog-cached-task-info
   :info 'duration
   :name task
   :range range))

(defun canrylog-task-duration-plus-children (tasks &optional range)
  "Return duration of TASKS and all its children.

TASKS can also be a task on its own.

If RANGE is a `canrylog-interval', only take into account the
amount of time TASKS have been active with RANGE."
  (let* ((tasks (ensure-list tasks))
         (tasks (mapcar #'canrylog::ensure-task-name tasks)))
    (canrylog-cached-task-info
     :info 'duration
     :name tasks
     :descendants t
     :range range)))

(defun canrylog-task-intervals-plus-children (tasks &optional range)
  "Return intervals of TASKS and its children (actually its descendants).

TASKS can also be a task on its own.

Intervals are returned from oldest to newest.

If RANGE is a `canrylog-interval', only return intervals within
RANGE, chopped off to fit within RANGE."
  (let* ((tasks (ensure-list tasks))
         (tasks (mapcar #'canrylog::ensure-task-name tasks))
         (intervals (canrylog-cached-task-info
                     :info 'intervals
                     :name tasks
                     :descendants t
                     :range range)))
    (--sort (time-less-p (oref it start)
                         (oref other start))
            intervals)))

(defun canrylog-task-running (task)
  "Return how long TASK has been active for, in seconds.

Return nil if TASK is not active."
  (let ((name (canrylog::ensure-task-name task))
        (latest-event (car (canrylog-get-events))))
    (when (equal name (canrylog-event-task latest-event))
      (canrylog-interval-duration
       (canrylog-interval :start (canrylog-event-moment latest-event)
                          :end nil)))))

(defun canrylog-task-exists? (task-or-name)
  "Does TASK-OR-NAME exist?

Existing means that it either has existing children, or itself
has been clocked into."
  (not (not (member (canrylog::ensure-task-name task-or-name)
                    (canrylog-get-task-names)))))

(defun canrylog-task-7-day-moving-average (task count)
  "Return the 7-day moving average of TASK for COUNT measures."
  (let (ret)
    (dotimes (i count)
      (message "%s" i)
      (push (canrylog-task-average-duration
                task
              :measure 86400
              :period (canrylog-interval-from-duration (* 7 86400))
              :offset i)
            ret))
    (nreverse ret)))

(cl-defun canrylog-task-average-duration
    (tasks &key
           (measure 86400)
           (period (canrylog-interval-from-duration (* 86400 30)))
           (offset 0))
  "How much time have I spent on TASKS each MEASURE during PERIOD?

By default, returns time spent on TASKS each day over the last 30 days.

If TASKS is actually a list of intervals, do the calculation on
those intervals instead.

When MEASURE is as long as PERIOD, this effectively returns the
total spent time during PERIOD with no averaging taking place.

If PERIOD is nil, calculate the average between the first time
the task is active. In case intervals were passed in instead of a
task, the interval list will be assumed to be sorted from newest
to oldest.

OFFSET means the end of the period is OFFSET measures from now in
the past. For example, if OFFSET 0 is 30 days ago to now (and
MEASURE is a day), then OFFSET 1 is 31 days ago to 1 day ago."
  (declare (indent 1))
  ;; doesn't make sense to limit range here
  (let* ((tasks (ensure-list tasks))
         (are-intervals (or (canrylog-interval-p (car tasks))))
         (period (or
                  period
                  (canrylog-interval
                   :start (if are-intervals
                              (oref (-last-item tasks) start)
                            (canrylog-task-first-active tasks))
                   :end nil))))
    (when offset
      (setq period
            (canrylog-interval
             :start (time-subtract (oref period start) (* offset measure))
             :end (time-subtract (oref period end) (* offset measure)))))
    (-> ; split steps to comment on them
     ;; How much of a fraction has TASK been active over PERIOD?
     (/ (if are-intervals
            (apply #'+ (--map (canrylog-interval-duration it period) tasks))
          (canrylog-task-duration-plus-children
           tasks period))
        (canrylog-interval-duration period)
        ;; don't want integer division
        1.0)
     ;; 1/3 * 86400 = 1/3 of a day in seconds
     (* measure))))

(defun canrylog-goal-matching (goal)
  "Return tasks of GOAL, including descendants if necessary."
  (let ((goal-tasks (canrylog-db-get-goal-tasks goal)))
    (-filter
     (lambda (task)
       (--any? (let ((goal-task (car it))
                     (include-descendants (cadr it)))
                 (or (and include-descendants
                          (canrylog::task-name:descendant-of-p task goal-task))
                     (equal task goal-task)))
               goal-tasks))
     (canrylog-get-task-names))))

;; FIXME: we assume weekly for now.
(defun canrylog-goal-realized (goal &optional week-offset)
  "Return the actual duration of GOAL's target tasks.
The range is the week specified by WEEK-OFFSET: 0 or nil is this
week, -1 is last week, and so on."
  (let ((tasks (canrylog-goal-matching goal))
        (week (canrylog-interval-from-name
               'week
               ;; nil = 0
               (or week-offset 0))))
    (if tasks
        (canrylog-cached-task-info
         :info 'duration
         :name tasks
         :range week)
      0)))

(provide 'canrylog-methods)
;;; canrylog-methods.el ends here
