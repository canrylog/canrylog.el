;;; canrylog-view.el --- User interfaces -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

(require 'cl-lib)
(require 'dash)
(require 'eieio)
(require 's)

(require 'canrylog-utils)
(require 'canrylog-evil)
(require 'canrylog-export)
(require 'canrylog-vars)
(require 'canrylog-types)
(require 'canrylog-methods)
(require 'canrylog-read)
(require 'canrylog-commands)
(require 'canrylog-db)
(require 'canrylog-buffers)

(defconst canrylog-view-sorting-methods
  `(("Last active"
     ,(-on #'canrylog-time-< #'canrylog-task-last-active)
     ,(-on #'canrylog-time->= #'canrylog-task-last-active)
     25)
    ("Duration"
     ,(-on #'< #'canrylog-task-duration-plus-children)
     ,(-on #'> #'canrylog-task-duration-plus-children)
     11)
    ("Name" string< string>)))

;;;; View

(cl-defmacro canrylog-define-view
    (name &key keymap
          major-mode-name major-mode-parent
          (major-mode-doc nil)
          (major-mode-hook nil)
          (major-mode-body nil)
          command-body (command-arglist nil) (command-doc nil)
          revert-body (revert-doc nil))
  "Define a Canrylog view called NAME.

A view consists of:
- a major mode, `canrylog-NAME-mode',
- its associated keymap, `canrylog-NAME-mode-map',
- its associated hook (optional), `canrylog-NAME-mode-hook',
- an entry command, `canrylog-view-NAME',
- a \"revert\" command responsible for actually rendering the view,
  `canrylog-view-NAME--revert'

A variable is also available in a view:
- `canrylog-view-state', alist for storing internal state, explained below.

The -BODY arguments, unlike `:config' in `use-package', need to
be wrapped in another level of parentheses. This is because I
gave up implementing `use-package'\\='s implicit `progn'. An extra
level parentheses may look awkward, but it makes it possible to
pass in `interactive' or `declare' forms to :command-body.

Default docstrings are used if -DOC arguments are not provided.

Internal state is handled by setting up
`canrylog-view-state' as a buffer local variable. You can
store your state into this variable with something
like (setf (alist-get \\='slot canrylog-view-state) t), and
access it with (alist-get \\='slot canrylog-view-state).

KEYMAP: passed to defvar at runtime to set as keymap for the major mode.

MAJOR-MODE-NAME, MAJOR-MODE-PARENT, MAJOR-MODE-DOC, MAJOR-MODE-BODY:
used to define the view\\='s major mode. Expands to:

  (define-derived-mode canrylog-NAME-mode ,major-mode-parent ,major-mode-name
    ,major-mode-doc
    (setq-local revert-buffer-function #\\=',canrylog-view-NAME--revert)
    ,@major-mode-body))

MAJOR-MODE-HOOK:
This is expanded such that the written list of functions are
added to the major mode's hook at runtime.

COMMAND-ARGLIST, COMMAND-BODY, COMMAND-DOC:
used to define the view\\='s main command. It\\='ll be called
canrylog-view-NAME.

REVERT-BODY, REVERT-DOC:
Used to define the view\\='s revert command. This is where the
actual updating of the view should be done. It\\='s a separate
function to make `revert-buffer' work."
  (declare (indent 1))
  (let ((var-keymap    (intern (format "canrylog-%s-mode-map" name)))
        (var-hook      (intern (format "canrylog-%s-mode-hook" name)))
        (var-mode      (intern (format "canrylog-%s-mode" name)))
        (var-command   (intern (format "canrylog-view-%s" name)))
        (var-revert    (intern (format "canrylog-view-%s--revert" name)))
        (keymap-doc (format "Keymap in %s view." name))
        ;; (buffer-doc (format "Buffer to show %s view in." name))
        (major-mode-doc (or major-mode-doc (format "Major mode for %s view." name)))
        (command-doc (or command-doc (format "Open %s view." name)))
        (revert-doc (or revert-doc (format "Actually render the %s view." name))))
    `(progn
       (defvar ,var-keymap ,keymap ,keymap-doc)
       (canrylog-evil--inhibit-insert-state ,var-keymap)
       (define-derived-mode ,var-mode ,major-mode-parent ,major-mode-name
         ,major-mode-doc
         :interactive nil
         (setq-local revert-buffer-function #',var-revert)
         ,@major-mode-body)
       ,(when major-mode-hook
          `(dolist (hook ,major-mode-hook)
             (add-hook ',var-hook hook)))
       (defun ,var-command ,command-arglist
         ,command-doc
         ,@command-body)
       (defun ,var-revert (&rest _)
         ,revert-doc
         ,@revert-body))))

(define-derived-mode canrylog-mode special-mode "Canrylog"
  "Major mode for the Canrylog UI.

\\{canrylog-mode-map}"
  (outline-minor-mode)
  (setq-local outline-regexp "*+")
  (setq-local imenu-space-replacement " ")
  (setq-local
   imenu-generic-expression
   `((nil
      ;; If we use a normal regexp here, imenu will try to skip our invisible
      ;; stars, leading to some (but for some reason not all) sections being
      ;; skipped. Using a function and doing the `re-search-backward' ourselves
      ;; allows us to not skip headings.
      ;;
      ;; See `imenu--generic-function' for reference, especially the part of it
      ;; doing the `re-search-backward'.
      ,(lambda ()
         (re-search-backward
          ;; From `outline-mode'
          (format "^\\(?:%s\\).*$" outline-regexp)
          nil t))
      0)))
  (setq-local truncate-lines t))
(when (featurep 'evil)
  (evil-set-initial-state 'canrylog-mode 'normal))

(defmacro canrylog-view-save-position (&rest body)
  "Run BODY, then restore line number and column.

The same line number and column are restored even if BODY
switched to a new buffer."
  `(let ((line (line-number-at-pos))
         (column (current-column)))
     (prog1 (progn ,@body)
       (goto-char (point-min))
       (ignore-errors
         (forward-line (1- line))
         (forward-char column)
         (recenter)))))

(defmacro canrylog-view-update (&rest body)
  "Run BODY with boilerplate to update views."
  `(let ((inhibit-read-only t))
     (canrylog-view-save-position
      (erase-buffer)
      ,@body)))

(cl-defmacro canrylog-view--make-button (label arguments &rest body)
  "Return a text button with LABEL that, when clicked, runs BODY.

ARGUMENTS is a plist containing the button\\='s properties. Keys can
be given as keywords, they will be converted to symbols.

This also sets \\='type of the button to \\='canrylog by default."
  (declare (indent 2))
  `(let ((label
          ;; This is needed as `make-text-button' modifies the string
          (copy-sequence ,label))
         (arguments (mapcar #'canrylog-keyword-to-symbol
                            ;; values in ARGUMENTS should be evaluated
                            (list ,@arguments))))
     (unless (plist-get arguments 'type)
       (plist-put arguments 'type 'canrylog))
     (plist-put arguments 'action (lambda (&rest _) ,@body))
     (apply #'make-text-button label nil arguments)))

(defun canrylog-view--insert-png-data (png)
  "Insert PNG data.
PNG is a string containing PNG bytes."
  (let ((image (create-image png 'png 'data
                             :width 200 :height 200
                             :scale 1
                             :max-width 200)))
    (image-flush image)
    (insert-image image)))

(cl-defmacro canrylog-view--insert-button (label arguments &rest body)
  "Insert a text button with LABEL that, when clicked, runs BODY.

Buttons inserted with this command are styled like ones that run
commands.

ARGUMENTS is a plist containing the button\\='s properties. Keys can
be given as keywords, they will be converted to symbols.

This also sets \\='type of the button to \\='canrylog by default."
  (declare (indent 2))
  `(let ((arguments (mapcar #'canrylog-keyword-to-symbol
                            ;; values in ARGUMENTS should be evaluated
                            (list ,@arguments))))
     (unless (plist-get arguments 'type)
       (setq arguments (plist-put arguments 'type 'canrylog)))
     (setq arguments (plist-put arguments 'action (lambda (&rest _) ,@body)))
     (apply #'insert-text-button (format "%s" ,label) arguments)))

;;;; Widget

(defclass canrylog-widget ()
  ((start :initform nil)
   (end :initform nil)
   (buffer :initform (current-buffer))))

(defmacro canrylog-widget--update (widget &rest body)
  "Update WIDGET with BODY.

Handles running BODY in WIDGET's buffer, as well as saving and
clearing its inserted region."
  (declare (indent 1))
  `(let ((inhibit-read-only t))
     (when (buffer-live-p (oref ,widget buffer))
       (with-current-buffer (oref ,widget buffer)
         (-when-let* ((start (oref ,widget start))
                      (end (oref ,widget end)))
           (delete-region start end)
           (goto-char start))
         (setf (oref ,widget start) (point))
         ,@body
         (setf (oref ,widget end) (point))))))

(defun canrylog-widget--revert-preserve-point (widget)
  "Revert WIDGET, preserving cursor position."
  ;; `save-excursion' ends up going back to the start inside of
  ;; `canrylog-widget--update' for some reason. This works.
  (let (prev-point prev-window-point)
    ;; The buffer might be displayed in a window, and that window
    ;; might be currently selected.
    ;;
    ;; 1. buffer is displayed in the current window
    ;;    - resetting `point' is enough
    ;; 2. buffer is displayed in another window
    ;;    - resetting `point' does not have an effect when switching back
    ;;    - need to reset `window-point' as well
    ;; 3. buffer is not displayed
    ;;    - resetting `point' is enough
    (setq prev-point (point))
    (let ((win (get-buffer-window (oref widget buffer))))
      (when win
        (setf prev-window-point (window-point win))))
    (canrylog-widget--revert widget)
    (goto-char prev-point)
    (let ((win (get-buffer-window (oref widget buffer))))
      (when win
        (setf (window-point win) prev-window-point)))))

(cl-defgeneric canrylog-widget--revert (_widget)
  "When there is no specialized revert function, just revert the entire buffer."
  (revert-buffer))

;;;;; Widget: main header
(defclass canrylog-widget-main-header (canrylog-widget)
  ((body :initarg :body)))

(cl-defmethod canrylog-widget--revert ((widget canrylog-widget-main-header))
  "Revert a periodically updated WIDGET."
  (canrylog-widget--update widget
    (let* ((current-task-name (canrylog-current-task-name))
           (downtime? (equal current-task-name canrylog-downtime-task)))
      (insert
       (canrylog::face 'canrylog-main-title
         (canrylog-format-duration
          (if downtime? 0 (canrylog-task-running current-task-name))))
       "\n")
      (unless downtime?
        (insert
         (canrylog::face 'canrylog-sans
           current-task-name)
         "\n\n"))
      (insert
       (if downtime?
           (concat
            (canrylog::face 'canrylog-sans
             "Clocked out.\n\n")
            (canrylog-widget-switch-task-button
             "Switch to task"
             "Switch to task"))
         (canrylog-view--make-button "Clock out"
             (:help-echo "Clock out")
           (canrylog-switch-task
            canrylog-downtime-task
            (current-time))))
       "\n\n"))))

;;;;; Widget: planning mode

;;;;; Widget: Children list

;; Could generalize this to task lists in general.
;; Using EIEIO to get local state.
(defclass canrylog-widget-children-list (canrylog-widget)
  ((task :initarg :task)
   (intervals :initarg :intervals)
   (sort :initform #'string<)))

(cl-defmethod canrylog-widget--revert ((widget canrylog-widget-children-list))
  "Revert the children list WIDGET."
  (let* ((task (oref widget task))
         (intervals (oref widget intervals))
         (own-intervals (canrylog-task-intervals-in-intervals task intervals))
         (children (canrylog-task-children task))
         (own-duration (apply #'+ (mapcar #'canrylog-interval-duration own-intervals))))
    (when children
      (canrylog-widget--update widget
        (insert (canrylog-widget-heading "Children"))
        (when (> (length children) 1)
          ;; Sorting buttons
          (pcase-dolist (`(,label ,desc ,asc ,width)
                         canrylog-view-sorting-methods)
            (let* ((current-sort (oref widget sort))
                   (label (cond ((equal current-sort desc)
                                 (concat label "(v)"))
                                ((equal current-sort asc)
                                 (concat label "(^)"))
                                (t label))))
              (insert
               (--> (canrylog-view--make-button label
                        (:help-echo
                         (format "Sort by %s" (downcase label))
                         :face
                         (if (member current-sort (list desc asc))
                             'canrylog-active-item
                           'canrylog-item))
                      (setf (oref widget sort)
                            (if (equal current-sort desc)
                                asc
                              desc))
                      (canrylog-widget--revert-preserve-point widget))
                    (if width (canrylog--ensure-width width it) it))))
            (insert " ")))
        (insert "\n")
        (when (> own-duration 0)
          (insert
           (concat
            (canrylog--ensure-width 26
              (canrylog-display-datetime (canrylog-task-last-active task)))
            (canrylog--ensure-width 12
              (canrylog-display-duration own-duration))
            "<immediate>"
            "\n")))
        ;; FIXME: we have two sources of durations here. The sorting doesn't use
        ;; this one.
        (let ((child-duration-table (canrylog-cached-task-descendants-duration task)))
          (dolist-with-progress-reporter
              (child (-sort (oref widget sort) children))
              (format "(canrylog) Displaying children of %s"
                      (canrylog::ensure-task-name task))
            (insert
             (format "%s  %-10s  %-15s"
                     (canrylog--ensure-width
                         24
                       (canrylog-display-datetime
                        (canrylog-task-last-active child)))
                     (canrylog-display-duration
                      (gethash child child-duration-table 0))
                     (canrylog-widget-task-button child))
             "\n")))))))

;;;;; Widget: Tag list
(defclass canrylog-widget-tags (canrylog-widget)
  ((task :initarg :task)))

(cl-defmethod canrylog-widget--revert ((widget canrylog-widget-tags))
  "Revert a tags WIDGET."
  (let* ((task (oref widget task))
         (tags (canrylog-task-tags task)))
    (canrylog-widget--update widget
      (insert "\n")
      (insert
       (canrylog-widget-heading "Tags"))
      (when tags
        (cl-loop for tag in (sort tags #'string<)
                 do (insert (canrylog-widget-tag-button tag) " ")
                 finally do (insert "\n")))
      (canrylog-view--insert-button "Add..."
          (:help-echo "Add a new tag")
        (let ((canrylog-read-task--force-task task))
          (call-interactively #'canrylog-task-add-tag)
          (canrylog-widget--revert-preserve-point widget)))
      (when tags
        (insert " ")
        (canrylog-view--insert-button "Delete..."
            (:help-echo "Delete a tag")
          (let ((canrylog-read-task--force-task task))
            (call-interactively #'canrylog-task-delete-tag)
            (canrylog-widget--revert-preserve-point widget))))
      (insert "\n"))))

;;;;; Widget: Metadata list
(defclass canrylog-widget-metadata (canrylog-widget)
  ((task :initarg :task)))

(cl-defmethod canrylog-widget--revert ((widget canrylog-widget-metadata))
  "Revert a metadata WIDGET."
  (let ((task (oref widget task)))
    (canrylog-widget--update widget
      (insert "\n")
      (insert
       (canrylog-widget-heading "Metadata"))
      (when-let ((metas (canrylog-task-metadata task)))
        (pcase-dolist (`(,key . ,value) metas)
          (canrylog-view--insert-button "Delete"
              (:face '(canrylog-dangerous canrylog-button)
               :help-echo "Remove this key")
            (canrylog-task-delete-metadata task key)
            (canrylog-widget--revert-preserve-point widget)
            (when (y-or-n-p (format "Deleted metadata %s. Revert? " key))
              (canrylog-task-set-metadata task key value)
              (canrylog-widget--revert-preserve-point widget)))
          (insert " ")
          (canrylog-view--insert-button "Edit..."
              (:help-echo "Change value of this key")
            (canrylog-task-set-metadata
             task key
             (read-string (format "Set %s to: " key)))
            (canrylog-widget--revert-preserve-point widget))
          (insert " ")
          (insert (canrylog-capitalize (symbol-name key))
                  ": " (format "%s" value) "\n")))
      (canrylog-view--insert-button "Add..."
          (:help-echo "Add a new metadata entry")
        (let ((canrylog-read-task--force-task task))
          (canrylog-task-set-metadata
           (canrylog-read-task)
           (read-string "New metadata entry: ")
           (read-string "Value: "))
          (canrylog-widget--revert-preserve-point widget)))
      (insert "\n"))))

(defun canrylog-view--percentage (value)
  "Return a string displaying VALUE as a colored percentage."
  (let ((percentage (* 100 value)))
    (canrylog::face (cond ((= percentage 0) 'error)
                          ((>= percentage 100) 'success)
                          (t 'warning))
      (format "%d%%" percentage))))

;;;; Display

(defun canrylog-widget-title (text &rest strings)
  "Widget for main title of a page saying TEXT.
STRINGS are appended onto TEXT."
  (canrylog::face 'canrylog-title
    (format "%s%s\n" text (string-join strings))))

(defun canrylog-widget-heading (text)
  "Return a heading saying TEXT."
  (canrylog::face 'canrylog-heading
    (concat (propertize "* " 'invisible t) text "\n")))

(defun canrylog-widget-task-button (task-name &optional label)
  "Return a button that visits TASK-NAME.

LABEL is TASK-NAME by default."
  (setq task-name (canrylog::ensure-task-name task-name))
  (let ((label (or label task-name)))
    (canrylog-view--make-button label
        (:type 'canrylog-item :help-echo "View more information about this task")
      (canrylog-view-task task-name))))

(defun canrylog-widget-task-duration (task &optional duration)
  "Return a line showing TASK and DURATION.

Calculate DURATION from TASK if it is nil."
  (unless duration
    (setq duration (canrylog-task-duration-plus-children task)))
  (format "%s  %s"
          (canrylog-widget-task-button task)
          (canrylog-display-duration duration)))

(defun canrylog-widget-tag-duration (tag &optional duration)
  "Return a line showing TAG and DURATION.

Calculate DURATION from TAG if it is nil."
  (unless duration
    (setq duration (canrylog-task-duration-plus-children
                    (canrylog-tag-tasks tag))))
  (format "%s  %s"
          (canrylog-widget-tag-button tag)
          (canrylog-display-duration duration)))

(defun canrylog-widget-tag-button (tag &optional label)
  "Return a button that opens the view for TAG.

LABEL is #TAG by default."
  (let ((label (or label (concat "#" tag))))
    (canrylog-view--make-button label
        (:type 'canrylog-item :help-echo "View more information about this tag")
      (canrylog-view-tag tag))))

(defun canrylog-widget-timestamp-button (time &optional label)
  "Return a button that visits TIME in the tracking file.

LABEL is by default TIME formatted according to `canrylog-datetime-format'."
  (let ((label (or label (canrylog-format-datetime time))))
    (canrylog-view--make-button label
        (:help-echo "View this timestamp in timetracking file"
         :type 'canrylog-item)
      (canrylog-view-timestamp time))))

(defclass canrylog-widget-intervals (canrylog-widget)
  ((intervals :initarg :intervals))
  "Interval list widget.")

(cl-defmethod canrylog-widget--revert ((widget canrylog-widget-intervals))
  "Render an intervals WIDGET."
  (let ((intervals (oref widget intervals)))
    (canrylog-widget--update widget
      (insert
       (->> intervals
            (--map
             (let ((active? (not (oref it end)))
                   (duration (canrylog-display-duration
                              (canrylog-interval-duration it))))
               (format
                "%s %-51s  %s%s\n"
                (canrylog-view--make-button
                    "view"
                    (:help-echo "View this interval in the tracking file"
                     :type 'canrylog-item)
                  (canrylog-view-interval it))
                (format "%s ~ %s"
                        (canrylog-format-datetime (oref it start))
                        (if (oref it end)
                            (canrylog-format-datetime (oref it end))
                          "now"))
                (if active?
                    (canrylog::face 'canrylog-duration "active")
                  (propertize duration 'help-echo "Duration"))
                (if active?
                    (concat
                     " "
                     (canrylog-view--make-button "edit"
                         (:help-echo "Change the start time of the active task"
                          :face 'canrylog-item)
                       (canrylog-with-file t
                         (goto-char (point-max))
                         (forward-char -1)
                         (call-interactively #'canrylog-file-change-time)
                         (canrylog-widget--revert widget))))
                  ""))))
            ;; I want the trailing newline
            (s-join ""))))))

(defun canrylog-widget-task-ancestry (task-name)
  "Insert the ancestry of TASK-NAME.

For example, given a task \"School:2018b:Course\", insert

    School / 2018b / Course

where \"School\" and \"2018b\" are both buttons to open the
corresponding task view."
  (concat
   (->> (canrylog::task-name:ancestors task-name)
        (--map (concat
                (canrylog::face '(canrylog-title canrylog-item)
                  (canrylog-widget-task-button
                   it (canrylog::task-name:base it)))
                (canrylog::face 'canrylog-title
                  ":")))
        (s-join ""))
   (canrylog::face 'canrylog-title
     (canrylog::task-name:base task-name))))

(defun canrylog-widget-switch-task-button (label &optional help-echo task)
  "Return a button with LABEL that, when clicked, switches tasks.

HELP-ECHO is the hover text.

If TASK is non-nil, make a button that switches to TASK instead
of prompting."
  (canrylog-view--make-button label
      (:help-echo (or help-echo "Switch to another task"))
    (if task
        (canrylog-switch-task task)
      (call-interactively #'canrylog-switch-task))
    (revert-buffer)))

(defun canrylog-widget-timeline (events)
  "Return a widget displaying EVENTS.

Remember to bind `canrylog-interval-duration--bound' to specify
when the end of the final interval should actually be."
  (let ((task+intervals (canrylog-tasks-in-events (reverse events))))
    (->> task+intervals
         (--map
          (let ((start-time (canrylog-display-time
                             (oref (cdr it) start)))
                (duration (canrylog-display-duration
                           (canrylog-interval-duration (cdr it)))))
            (concat
             (--> (format "%s %s"
                          start-time
                          (canrylog-widget-task-button (car it)))
                  (canrylog--ensure-width
                      (min (- (window-width)
                              (string-width duration))
                           60)
                    it))
             duration
             "\n")))
         ;; Start from most recent to least recent
         reverse
         ;; I want the trailing newline
         (s-join ""))))

(defun canrylog--distribution-to-top-level (distribution)
  "Group tasks DISTRIBUTION by their top level name.
DISTRIBUTION is an alist of task names to durations."
  (let ((result))
    (--each distribution
      (let ((key (intern (canrylog::task-name:top-level (car it)))))
        (setf (alist-get key result)
              (+ (alist-get key result 0)
                 (cdr it)))))
    (->> result
         (--sort (> (cdr it) (cdr other)))
         (--map (cons (symbol-name (car it))
                      (cdr it))))))

(defun canrylog-view--frequency-goal-format-relative (difference seconds)
  "Format DIFFERENCE according to the frequency goal SECONDS.
If DIFFERENCE is more than SECONDS, apply the correct color."
  (let* ((too-long (and seconds (> difference seconds)))
         (humanized (canrylog-time-humanize difference))
         (readable (if (string-empty-p humanized)
                       "just now"
                     (format "%s ago" humanized))))
    (cond
     ;; No goal - no face
     ((not seconds) readable)
     ;; With goal but too long - error face
     (too-long (canrylog::face 'error readable))
     ;; With goal and not too long - success face
     (t (canrylog::face 'success readable)))))

;;;; Possibly reused widgets

(defun canrylog-view--insert-task-hierarchy (&optional _root)
  "Insert the hierarchy under task ROOT.

When ROOT is nil, show all tasks."
  (insert (s-trim "
Imagine           00:02:00
  A hierarchy     00:00:30
    *immediate*   00:00:10
    doing stuff   00:00:20
  Here            00:01:00")
          "\n"))

(defun canrylog-view--insert-flat-task-list (&optional sorting-method)
  "Insert the flat task overview.

Optional SORTING-METHOD indicates the way tasks are sorted.
`duration' means to sort by task duration, shortest to longest;
`duration-decending' means to sort by longest to shortest;
`name-decending' means to sort by task name, Z to A;
`name' means to sort by name, A to Z;
`recent-duration' means to sort by average daily duration in the
last 30 days, shortest to longest;
`recent-duration-decending' means the same but from longest to
shortest."
  (let ((sort-func
         ;; A and B are both cons cells (TASK . DURATION).
         (pcase sorting-method
           (`duration
            (lambda (a b)
              (< (cdr a)
                 (cdr b))))
           (`duration-decending
            (lambda (a b)
              (> (cdr a)
                 (cdr b))))
           (`name
            (lambda (a b)
              (string< (car a)
                       (car b))))
           (`name-decending
            (lambda (a b)
              (string> (car a)
                       (car b))))
           (`recent-duration
            (lambda (a b)
              (< (canrylog-task-average-duration (car a))
                 (canrylog-task-average-duration (car b)))))
           (`recent-duration-decending
            (lambda (a b)
              (> (canrylog-task-average-duration (car a))
                 (canrylog-task-average-duration (car b))))))))
    (let (events distribution)
      (canrylog::loading "Getting events..."
        (setq events (reverse (canrylog-get-events))))
      (canrylog::loading "Calculating durations..."
        (setq distribution (canrylog--events-distribution events)))
      (canrylog::loading "Sorting tasks..."
        (setq distribution (-sort sort-func distribution)))
      (cl-loop
       with reporter = (make-progress-reporter "Listing tasks" 0 (length distribution))
       with count = 0
       for (task . duration) in distribution
       do
       (progn
         (insert
          (format "%s  %s\n"
                  (canrylog-widget-task-button task)
                  (canrylog-display-duration duration)))
         (progress-reporter-update reporter (cl-incf count)))
       finally (progress-reporter-done reporter)))))

(defun canrylog-view--insert-commands (&rest commands)
  "Insert commands and their key bindings.

COMMANDS is a plist mapping the function name to a description.

Example:

  (canrylog-view--insert-commands
    #\\='canrylog-view-task \"Describe a task\"
    #\\='canrylog-switch-task \"Switch to task...\")"
  (mapcar
   (pcase-lambda (`(,cmd ,desc))
     (progn (canrylog--insert-html
             (substitute-command-keys
              (format "<b>\\[%s]</b>: " cmd)))
            (canrylog-view--insert-button desc nil
              (call-interactively cmd))
            (insert "\n")))
   (-partition 2 commands)))

(defun canrylog-view--insert-back-to-listing ()
  "Insert button that views the task list."
  (canrylog-view--insert-button "Back to listing"
      (:help-echo "Go back to task listing")
    (canrylog-view-task-list)))

(defun canrylog-view--insert-task-stats (tasks &optional tag)
  "Insert stats for TASKS.
If TAG is non-nil, indicate that the stats are for TAG in the
displayed strings."
  (setq tasks (ensure-list tasks))
  (canrylog::message "Reading task intervals...")
  (let ((intervals (canrylog-cached-task-info
                    :info 'intervals :name tasks :descendants t)))
    ;; This is way too slow.
    ;; (-some--> (canrylog::python-plot 'line
    ;;             (json-encode (canrylog-task-7-day-moving-average tasks 7)))
    ;;   (canrylog-view--insert-png-data it)
    ;;   (insert "\n"))
    (insert (format "Total duration: %s"
                    (canrylog-format-duration
                     (apply #'+ (mapcar #'canrylog-interval-duration intervals))))
            "\n")
    (insert (format "Last 7 days: %s"
                    (canrylog-format-duration
                     (canrylog-task-average-duration intervals
                       :measure (* 86400 7)
                       :period (canrylog-interval-from-duration
                                (* 86400 7)))))
            "\n")
    (insert (format
             "%s: %s"
             (canrylog-view--make-button "This week"
                 (:help-echo (if tag
                                 "View stats for this tag"
                               "View stats for this task")
                  :face 'canrylog-item)
               (canrylog-view-task-stats tasks tag 'week))
             (canrylog-format-duration
              (canrylog-task-duration-plus-children
               tasks
               (canrylog-interval-from-name 'this-week))))
            "\n")
    (insert (format
             "%s: %s"
             (canrylog-view--make-button
                 (format "This month (%s)"
                         (format-time-string "%B %Y"))
                 (:help-echo (if tag
                                 "View stats for this tag"
                               "View stats for this task")
                  :face 'canrylog-item)
               (canrylog-view-task-stats tasks tag 'month))
             (canrylog-format-duration
              (canrylog-task-duration-plus-children
               tasks
               (canrylog-interval-from-name 'this-month))))
            "\n")
    (insert (format "Daily average in the last 7 days: %s"
                    (canrylog-format-duration
                     (canrylog-task-average-duration intervals
                       :measure 86400
                       :period (canrylog-interval-from-duration
                                (* 86400 7)))))
            "\n")
    (insert (format "Daily average in the last 30 days: %s"
                    (canrylog-format-duration
                     (canrylog-task-average-duration intervals
                       :measure 86400
                       :period (canrylog-interval-from-duration
                                (* 86400 30)))))
            "\n")
    (insert (format "Daily average since first active: %s"
                    (canrylog-format-duration
                     (canrylog-task-average-duration intervals
                       :measure 86400
                       :period nil)))
            "\n")))

(cl-defun canrylog-view--insert-distribution (title &key distribution png)
  "Insert a distribution section titled as TITLE.
DISTRIBUTION is an alist mapping task names to durations.

If PNG is non-nil, assume it is PNG data visualizing the
distribution."
  (declare (indent 1))
  (insert
   (canrylog-widget-heading title))
  (when png
    (canrylog-view--insert-png-data png)
    (insert "\n"))
  (--each distribution
    (insert (canrylog-widget-task-duration (car it) (cdr it))
            "\n")))

;;;; Misc.

(cl-defmacro canrylog-with-ui ((&key buffer display) &rest body)
  "Run BODY in a UI buffer, and return its result.

BUFFER: the buffer to run BODY in.
DISPLAY: whether to display BUFFER. Possible values:
  `:after': display buffer after running BODY
  Any other non-nil value: do so before running BODY"
  (declare (indent 1))
  `(let ((buf (get-buffer-create ,buffer)))
     (canrylog-buffers--push buf)
     ,@(when (and display (not (eq display :after)))
         `((canrylog--pop-to-buffer ,buffer)))
     (with-current-buffer buf
       (canrylog-mode)
       (prog1 (let ((inhibit-read-only t))
                ,@body)
         ,@(when display
             `((goto-char (point-min))))))
     ,@(when (eq display :after)
         `((canrylog--pop-to-buffer ,buffer)))))

;;;; Views
;;;;; "Interval view" / "Timestamp view" = visiting the line in the tracking file
(defun canrylog-view-interval (interval)
  "View INTERVAL by visiting the relevant line in the tracking file."
  (canrylog-view-timestamp (oref interval start)))

(defun canrylog-view-timestamp (timestamp)
  "View TIMESTAMP by visiting the relevant line in the tracking file."
  (find-file (canrylog-file))
  (canrylog-file-goto-moment timestamp))

;;;;; Export view
(canrylog-define-view "export"
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map "?" #'canrylog-metadata-dispatch)
            map)
  :major-mode-name "Canrylog-Export"
  :major-mode-parent canrylog-mode
  :major-mode-hook (list #'goto-address-mode)
  :command-arglist (task-name)
  :command-body
  ((interactive (list (canrylog-read-task "Export task: " :require-match t)))
   (canrylog-with-ui (:buffer "*Canrylog Task Export*"
                      :display t)
     (canrylog-export-mode)
     (canrylog--vset task-name task-name)
     (canrylog--vset format 'tsv)
     (canrylog--vset daily-weekly 'daily)
     (revert-buffer)))
  :revert-body
  ((canrylog--vlet (task-name format daily-weekly)
     (cl-assert task-name nil "Missing name of task, it's not properly set")
     (canrylog-view-update
      (insert
       (canrylog::face 'canrylog-title
         "Export task: ")
       (canrylog::face '(canrylog-title canrylog-item)
         (canrylog-widget-task-button task-name))
       "\n")
      (canrylog-view--insert-button "Change task"
          (:help-echo "Select another task to export")
        (call-interactively #'canrylog-view-export))
      (insert "\n\n")
      (insert
       (format "Format: %s %s\n"
               (if (eq format 'csv)
                   (canrylog::face '(bold canrylog-sans) "CSV")
                 (canrylog-view--make-button "CSV"
                     (:help-echo "Set format to CSV")
                   (canrylog--vset format 'csv)
                   (revert-buffer)))
               (if (eq format 'tsv)
                   (canrylog::face '(bold canrylog-sans) "TSV")
                 (canrylog-view--make-button "TSV"
                     (:help-echo "Set format to TSV")
                   (canrylog--vset format 'tsv)
                   (revert-buffer)))))
      (canrylog-view--insert-button "Export daily durations"
          (:help-echo "Export durations of the task for each day")
        (canrylog-export-task-duration
         task-name
         :file (read-directory-name "Export to: " nil nil t)
         :format (or format 'tsv)
         :hour t))
      (insert "\n\n")
      (insert
       (format "Daily or weekly: %s %s\n"
               (if (eq daily-weekly 'daily)
                   (canrylog::face '(bold canrylog-sans) "Daily")
                 (canrylog-view--make-button "Daily"
                     (:help-echo "Report Daily")
                   (canrylog--vset daily-weekly 'daily)
                   (revert-buffer)))
               (if (eq daily-weekly 'weekly)
                   (canrylog::face '(bold canrylog-sans) "Weekly")
                 (canrylog-view--make-button "Weekly"
                     (:help-echo "Report Weekly")
                   (canrylog--vset daily-weekly 'weekly)
                   (revert-buffer)))))
      (canrylog-view--insert-button "Show duration stats on Xmrit"
          (:help-echo "Export daily duration and view as XmR chart")
        (canrylog-export-task-duration-xmrit task-name :type daily-weekly))
      (insert "\n\n")
      (canrylog-view--insert-button "Back to task view"
          (:help-echo "Go back to viewing information about this task")
        (canrylog-view-task task-name))))))

;;;;; Task view
(canrylog-define-view "task"
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map "?" #'canrylog-metadata-dispatch)
            map)
  :major-mode-name "Canrylog-Task"
  :major-mode-parent canrylog-mode
  :major-mode-hook (list #'goto-address-mode)
  :command-arglist (task-name)
  :command-body
  ((interactive
    (list (canrylog-read-task "Describe task: ")))
   (canrylog-with-ui (:buffer (format "*Canrylog Task: %s*"
                                      task-name)
                      :display t)
     (canrylog-task-mode)
     (canrylog--vset task task-name)
     (revert-buffer)))
  :revert-body
  ((canrylog--vlet (task)
     (cl-assert task nil "Missing name of task, it's not properly set")
     (canrylog-view-update
      (insert
       (canrylog-widget-task-ancestry task))
      (insert "\n\n")
      (if (equal task (canrylog-current-task-name))
          (insert (canrylog-widget-switch-task-button
                   "Switch to another task"
                   "Switch to another task"))
        (canrylog-view--insert-button "Switch to this task"
            (:help-echo "Switch to this task")
          (canrylog-switch-task task (current-time))))
      (insert "\n")
      (canrylog-view--insert-button "Rename this task"
          (:help-echo "Rename this task, keeping its children.")
        (let ((new-name (canrylog-read-new-task "Rename to: " task)))
          (canrylog-rename-task-keep-children task new-name)
          (canrylog-view-task new-name)))
      (insert " ")
      (canrylog-view--insert-button "Rename this task (detach children)"
          (:help-echo "Rename this task but not its children.")
        (let ((new-name (canrylog-read-new-task "Rename to: " task)))
          (canrylog-rename-task task new-name)
          (canrylog-view-task new-name)))
      (insert "\n")
      (canrylog-view--insert-button "Export..."
          (:help-echo "Export options")
        (canrylog-view-export task))
      (insert "\n")
      (canrylog-view--insert-button "Edit frequency goal..."
          (:help-echo "Edit how often this task should be active")
        (let ((canrylog-read-task--force-task task))
          (call-interactively #'canrylog-db-set-frequency-goal)
          (revert-buffer)))
      (insert "\n\n")
      (let ((description (canrylog-task-description task)))
        (when description
          (insert (canrylog::face 'canrylog-sans description)
                  "\n\n"))
        (if description
            (canrylog-view--insert-button "Edit description..."
                (:help-echo "Edit the description for this task")
              (let ((new (read-string-from-buffer
                          "New description:" description)))
                (canrylog-task-set-description task new))
              (revert-buffer))
          (canrylog-view--insert-button "Add description..."
              (:help-echo "Add a description for this task")
            (let ((new (read-string-from-buffer "New description:" "")))
              (canrylog-task-set-description task new))
            (revert-buffer))))
      (insert "\n\n"
              (canrylog-widget-heading
               "Stats"))
      (let ((last-active (canrylog-task-last-active task nil t)))
        (cond
         ((eq last-active 'never)
          (insert "Last active: never\n"))
         (last-active
          (let* ((difference (abs ; we care about the difference only
                              (- (canrylog-time-coerce--unix (float-time))
                                 (canrylog-time-coerce--unix last-active))))
                 (frequency-goal (canrylog-db-get-frequency-goal task)))
            (insert
             (format
              "Last active: %s (%s)\n"
              (canrylog-widget-timestamp-button last-active)
              (canrylog-view--frequency-goal-format-relative
               difference frequency-goal)))
            (when frequency-goal
              (insert
               (format "  Frequency goal: no more than %s\n"
                       (canrylog-time-humanize frequency-goal))))))
         (t
          (insert "Last active: now\n"))))
      (canrylog-view--insert-task-stats task)
      (canrylog-widget--revert
       (canrylog-widget-tags :task task))
      (canrylog-widget--revert
       (canrylog-widget-metadata :task task))
      (insert "\n")
      (let* ((intervals
              (canrylog-cached-task-info
               :info 'intervals
               :name (list task)
               :descendants t))
             (own-intervals
              (progn
                (canrylog::message "Filtering intervals...")
                (canrylog-task-intervals-in-intervals task intervals))))
        (canrylog-widget--revert
         (canrylog-widget-children-list
          :task task :intervals intervals))
        (when own-intervals
          (insert "\n" (canrylog-widget-heading "Intervals"))
          (canrylog-widget--revert
           (canrylog-widget-intervals :intervals own-intervals))))
      (insert "\n\n")
      (canrylog-view--insert-back-to-listing)))))

(defalias 'canrylog-describe-task #'canrylog-view-task)

(canrylog-define-view "tag"
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map "l" #'canrylog-view-task-list)
            (define-key map "s" #'canrylog-switch-task)
            (define-key map "v" #'canrylog-visit)
            (define-key map "r" #'canrylog-view-main)
            map)
  :major-mode-name "Canrylog-Tag"
  :major-mode-parent canrylog-mode
  :major-mode-hook (list #'goto-address-mode)
  :command-arglist (tag)
  :command-body
  ((interactive
    (list (canrylog-read-tag "Describe tasks with tag: ")))
   (cl-assert tag nil "Missing name of tag")
   (canrylog-with-ui (:buffer (format "*Canrylog Tag: #%s*"
                                      tag)
                      :display t)
     (canrylog-tag-mode)
     (canrylog--vset tag)
     (revert-buffer)))
  :revert-body
  ((let* ((tag (alist-get 'tag canrylog-view-state))
          (tasks (-sort #'string< (canrylog-tag-tasks tag)))
          (distribution
           ;; This has every descendant task as their own items
           ;; I want the items to be the ones directly tagged
           (canrylog-cached-task-info
            :info 'distribution
            :name tasks
            :descendants t))
          direct-distribution)
     (dolist (direct-task tasks)
       (pcase-dolist (`(,task . ,dur) distribution)
         (when (or (equal task direct-task)
                   (canrylog::task-name:descendant-of-p task direct-task))
           (setf
            (alist-get direct-task direct-distribution 0 nil #'equal)
            (+ dur
               (alist-get direct-task direct-distribution 0 nil #'equal))))))
     (canrylog-view-update
      (insert
       (canrylog-widget-title (format "Report for #%s" tag))
       "\n")
      (canrylog::loading "Getting task durations..."
        (canrylog-view--insert-distribution
            (format "Tasks tagged with #%s" tag)
          :distribution
          (--sort (> (cdr it) (cdr other))
                  direct-distribution))
        (insert "\n"))
      (canrylog::loading "Getting stats..."
        (insert (canrylog-widget-heading "Stats"))
        (canrylog-view--insert-task-stats tasks tag))))))

;;;;; Task / tag stats
(canrylog-define-view "task-stats"
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map "l" #'canrylog-view-task-list)
            (define-key map "s" #'canrylog-switch-task)
            (define-key map "v" #'canrylog-visit)
            (define-key map "r" #'canrylog-view-main)
            map)
  :major-mode-name "Canrylog-Task-Stats"
  :major-mode-parent canrylog-mode
  :major-mode-hook (list #'goto-address-mode)
  :command-arglist (tasks &optional tag period)
  :command-doc "Show stats for PERIOD for TASKS.
PERIOD can be `day', `week', or `month'. If nil, the default is `week'.
If TAG is non-nil, indicate in the displayed text that it's
actually reporting for TAG."
  :command-body
  ((cl-assert tasks nil "Missing tasks")
   (let ((reportee (or (and tag (format "#%s" tag))
                       (car tasks))))
     (canrylog-with-ui (:buffer (format "*Canrylog Task Stats: %s*"
                                        reportee)
                        :display t)
       (canrylog-task-stats-mode)
       (canrylog--vset tasks)
       (canrylog--vset reportee)
       (canrylog--vset tag)
       (canrylog--vset period (or period 'week))
       (revert-buffer))))
  :revert-body
  ((canrylog--vlet (tasks reportee period tag)
     (canrylog-view-update
      (insert
       (canrylog-widget-title
        (pcase period
          (`week (format "Weekly stats for %s" reportee))
          (`month (format "Monthly stats for %s" reportee))
          (`day (format "Daily stats for %s" reportee))))
       "\n")
      (canrylog-view--insert-button (format "Back to info for %s" reportee)
          ()
        (if tag
            (canrylog-view-tag tag)
          (canrylog-view-task reportee)))
      (insert "\n\n")
      (dotimes-with-progress-reporter (i 20) "(canrylog) Calculating duration..."
        (let ((interval (canrylog-interval-from-name period (- i))))
          (insert
           (pcase period
             (`month (-->
                      (canrylog-display-month (oref interval start))
                      (canrylog-view--make-button it
                          (:help-echo (format "View report for %s" it)
                           :face 'canrylog-item)
                        (canrylog-view-report 'month (- i)))
                      (format "%s: " it)))
             (`week (--> (format "%s ~ %s: "
                                 (canrylog-format-date (oref interval start))
                                 (canrylog-format-date (oref interval end)))
                         (canrylog-view--make-button it
                             (:help-echo (format "View report for %s" it)
                              :face 'canrylog-item)
                           (canrylog-view-report 'week (- i)))))
             (_ (format "%s ~ %s: "
                        (canrylog-format-date (oref interval start))
                        (canrylog-format-date (oref interval end)))))
           (canrylog-format-duration
            (canrylog-task-duration-plus-children tasks interval))
           "\n")))))))

;;;;; Main view
(canrylog-define-view "main"
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "l") #'canrylog-view-task-list)
            (define-key map (kbd "s") #'canrylog-switch-task)
            (define-key map (kbd "R") #'canrylog-view-report)
            (define-key map (kbd "v") #'canrylog-visit)
            (define-key map (kbd "t") #'canrylog-view-task)
            map)
  :major-mode-name "Canrylog-Main"
  :major-mode-parent canrylog-mode
  :major-mode-body
  ((when (featurep 'evil)
     (evil-normalize-keymaps)))
  :command-body
  ((interactive)
   (canrylog-with-ui (:buffer "*Canrylog*" :display t)
     (canrylog-main-mode)
     (revert-buffer)))
  :revert-body
  ((canrylog-view-update
    (let* ((header-widget (canrylog-widget-main-header)))
      (canrylog-widget--revert header-widget)
      (insert
       (canrylog-widget-heading "Task shortcuts"))
      ;; FIXME: hard coded task shortcuts
      (let ((current-task (canrylog-current-task-name)))
        (--each '("Sleep" "Skill:Trumpet")
          (insert (if (equal it current-task)
                      it
                    (canrylog-widget-switch-task-button
                     it
                     (format "Switch to %s" it)
                     it))
                  " ")))
      (insert "\n \n")
      (insert
       (canrylog-widget-heading "Reports")
       (canrylog-view--make-button "View report for today"
           (:help-echo "Open today's report")
         (canrylog::loading "Opening daily report..."
           (canrylog-view-report 'day 0)))
       "\n"
       (canrylog-view--make-button "View report for this week"
           (:help-echo "Open report for this week")
         (canrylog::loading "Opening weekly report..."
           (canrylog-view-report 'week 0)))
       "\n"
       (canrylog-view--make-button "View report for this month"
           (:help-echo "Open report for this month")
         (canrylog::loading "Opening monthly report..."
           (canrylog-view-report 'month 0)))
       "\n")
      (insert "\n")
      (canrylog::loading "Inserting frequency goals..."
        (insert
         (canrylog-widget-heading "Habit tracking"))
        (canrylog-view-insert-table
         '(("Task" . 26)
           ("Last active" . 15)
           ("Should do every" . 20))
         (->>
          (canrylog-db-list-frequency-goals)
          (--map
           (-let* (((task seconds) it)
                   (last-active-relative (canrylog-task-last-active-relative task t)))
             (list
              (canrylog-widget-task-button task)
              (cond ((eq last-active-relative 'now)
                     (canrylog::face 'success "now"))
                    ((eq last-active-relative 'never)
                     (canrylog::face 'error "never"))
                    (t
                     (canrylog-view--frequency-goal-format-relative
                      last-active-relative seconds)))
              (canrylog-view--make-button
                  (canrylog-time-humanize seconds)
                  (:help-echo "Set target interval")
                (let ((canrylog-read-task--force-task task))
                  (call-interactively #'canrylog-db-set-frequency-goal)
                  (revert-buffer)))))))))
      (insert "\n")
      (canrylog::loading "Inserting goals..."
        (insert
         (canrylog-widget-heading "Goals and progress for this week"))
        (canrylog-view--insert-goals 0)
        (insert
         (canrylog-view--make-button "View goals"
             (:help-echo "Go to the goals view")
           (canrylog-view-planning))
         "\n\n"))
      (canrylog::loading "Inserting events..."
        (dolist (offset '(0 -1 -2))
          (let ((events (canrylog-with-file nil
                          (canrylog-parse-events-interval
                           (canrylog-interval-from-name 'day offset)))))
            (when events
              (insert
               (canrylog-widget-heading
                (canrylog-display-day
                 (canrylog--today offset)))
               (canrylog-widget-timeline (reverse events))
               "\n")))))))))

;;;;; Task list view
(defconst canrylog-view-task-list-sorting-methods
  '((duration . "Total duration (shortest first)")
    (duration-decending . "Total duration (longest first)")
    (name . "Name (A-Z)")
    (name-decending . "Name (Z-A)")
    (recent-duration . "Recent duration (last 30 days) (shortest first)")
    (recent-duration-decending . "Recent duration (last 30 days) (longest first)"))
  "Alist describing sorting methods for the flat task list.

See `canrylog-view--insert-flat-task-list' for what they mean.")

;; FIXME: we should store tasks and their sorting information locally
;; first before rendering them. This allows changing sorts to not
;; require re-parsing everything.
;;
;; TODO: Integrate goals, show matching goals for tasks?
;; TODO: Top-level view where you only show top level tasks, and/or expanding
;; levels per item
;; TODO: "Limit to this week"
(canrylog-define-view "task-list"
  :keymap
  (let ((map (make-sparse-keymap)))
    (define-key map "l" #'canrylog-view-task-list)
    (define-key map "v" #'canrylog-visit)
    map)
  :major-mode-name "Canrylog-Task-List"
  :major-mode-parent canrylog-mode
  :command-arglist ()
  :command-body
  ((interactive)
   (canrylog-with-ui (:buffer "*Canrylog Task List*" :display t)
     ;; TODO: implement command to switch between the two
     (canrylog-task-list-mode)
     (canrylog--vset mode 'flat)
     (canrylog--vset sort 'name)
     (revert-buffer)))
  :revert-body
  ((canrylog-view-update
    (canrylog--vlet (sort mode)
      (insert
       (canrylog-widget-title "All tasks")
       "\n"
       (canrylog::face 'canrylog-sans "Sorting by: ")
       (canrylog-view--make-button
           (alist-get sort canrylog-view-task-list-sorting-methods)
           (:help-echo "Change sorting method")
         (let ((new-method (--> (completing-read "Sort by: "
                                                 (map-values canrylog-view-task-list-sorting-methods)
                                                 nil t)
                                (rassoc it canrylog-view-task-list-sorting-methods)
                                (car it))))
           (canrylog--vset sort new-method)
           (message "Sorting by %s..."
                    (alist-get new-method canrylog-view-task-list-sorting-methods))
           (revert-buffer))))
      (insert "\n\n")
      (pcase mode
        (`hierarchy (canrylog-view--insert-task-hierarchy))
        (`flat (canrylog-view--insert-flat-task-list sort)))))))

;;;;; Report view

(canrylog-define-view "report"
  :keymap (let ((map (make-sparse-keymap)))
            map)
  :major-mode-name "Canrylog-Report"
  :major-mode-parent canrylog-mode
  :command-arglist (type offset)
  :command-body
  ((interactive (list 'day 0))
   (canrylog-with-ui (:buffer (format "*Canrylog Report: %s*"
                                      (pcase type
                                        (`day
                                         (canrylog--today offset))
                                        (`month
                                         (canrylog-format-month
                                          (oref
                                           (canrylog-interval-from-name 'month offset)
                                           start)))
                                        (`week
                                         (canrylog-format-week
                                          (canrylog-interval-from-name 'week offset)))))
                      :display t)
     (canrylog-report-mode)
     (canrylog--vset type)
     (canrylog--vset offset)
     (revert-buffer)))
  :revert-body
  ((canrylog-view-update
    (let* ((type (alist-get 'type canrylog-view-state))
           (offset (alist-get 'offset canrylog-view-state))
           (this-interval
            (canrylog-interval-from-name type offset))
           (this-interval
            (if (= offset 0)
                (canrylog-interval
                 :start (oref this-interval start)
                 :end (current-time))
              this-interval))
           (distribution
            (-some->> (canrylog-parse-task-info
                       :info 'distribution
                       :range this-interval)
              (--sort (> (cdr it) (cdr other))))))
      (insert
       (canrylog-widget-title
        (pcase type
          (`day
           (format (if (= offset 0)
                       "Report for today (%s)"
                     "Report for %s")
                   (canrylog-display-day (canrylog--today offset))))
          (`week
           (format (if (= offset 0)
                       "Report for this week (%s)"
                     "Report for week: %s")
                   (canrylog-format-week
                    (canrylog-interval-from-name 'week offset))))
          (`month
           (format (if (= offset 0)
                       "Report for this month (%s)"
                     "Report for %s")
                   (canrylog-display-month
                    (oref (canrylog-interval-from-name 'month offset)
                          start))))))
       "\n")
      (canrylog-view--insert-button
          "Go to date..."
          (:help-echo "Select a day and view its daily report")
        (let ((new-offset (canrylog--days-between
                           (canrylog-read-date
                            "View report for another day: ")
                           (canrylog--today))))
          (if (> new-offset 0)
              (message "Cannot report on the future")
            (canrylog-view-save-position
             (canrylog-view-report type new-offset)))))
      (insert "\n")
      (canrylog-view--insert-button
          (pcase type
            (`day "Previous day")
            (`week "Last week")
            (`month "Last month"))
          (:help-echo "View report for the previous period")
        (canrylog-view-save-position
         (canrylog-view-report
          type
          (1- offset))))
      (unless (= offset 0)
        (insert "\n")
        (canrylog-view--insert-button
            (pcase type
              (`day "Next day")
              (`week "Next week")
              (`month "Next month"))
            (:help-echo "View report for the next period")
          (canrylog-view-save-position
           (canrylog-view-report
            type
            (1+ offset)))))
      (insert "\n\n")
      (canrylog::loading "Getting tag durations..."
        (when-let ((tag-distribution
                    (-some->> (canrylog-db-get-tags-list)
                      (--map (cons it (canrylog-task-duration-plus-children
                                       (canrylog-tag-tasks it)
                                       this-interval)))
                      (--filter (> (cdr it) 0)))))
          (insert (canrylog-widget-heading "Tags"))
          (--each tag-distribution
            (insert (canrylog-widget-tag-duration (car it) (cdr it))
                    "\n"))
          (insert "\n")))
      (if distribution
          (let ((top-level-distribution (canrylog--distribution-to-top-level distribution))
                each-png top-level-png)
            (setq each-png (canrylog::python-plot 'pie
                             (json-encode distribution)))
            (setq top-level-png (canrylog::python-plot 'pie
                                  (json-encode top-level-distribution)))
            (canrylog-view--insert-distribution "Distribution per top level task"
              :distribution top-level-distribution
              :png top-level-png)
            (insert "\n")
            (canrylog-view--insert-distribution "Distribution"
              :distribution distribution
              :png each-png))
        (insert
         (canrylog::face 'canrylog-sans
           "No entries logged for this day.")))))))

;;;;; Time allocation view

;; Future plan: integrate goals into the main task list view
(canrylog-define-view "planning"
  :keymap (let ((map (make-sparse-keymap)))
            map)
  :major-mode-name "Canrylog-Planning"
  :major-mode-parent canrylog-mode
  :command-arglist ()
  :command-body
  ((interactive)
   (canrylog-with-ui (:buffer (format "*Canrylog Planning*")
                      :display t)
     (canrylog-planning-mode)
     (revert-buffer)))
  :revert-body
  ((canrylog-view-update
    (canrylog--vlet (week-offset)
      (insert
       (if (= 0 (or week-offset 0))
           (canrylog-widget-title "Goals and progress for this week")
         (canrylog-widget-title
          (let ((interval (canrylog-interval-from-name
                           'week
                           (or week-offset 0))))
            (format "Goals and progress for the week\nbetween %s\nand %s"
                    (format-time-string
                     "%F (%A)"
                     (oref interval start))
                    (format-time-string
                     "%F (%A)"
                     (oref interval end)))))))
      (insert "\n")
      (canrylog-view--insert-button "Last week"
          (:help-echo "View goals report for last week")
        (canrylog--vset week-offset
          (1- (or week-offset 0)))
        (revert-buffer))
      (unless (= 0 (or week-offset 0))
        (insert " ")
        (canrylog-view--insert-button "Next week"
            (:help-echo "View goals report for next week")
          (canrylog--vset week-offset
            (1+ (or week-offset 0)))
          (revert-buffer)))
      (insert "\n\n")
      (canrylog-view--insert-goals week-offset)
      (insert "\n")
      (canrylog-view--insert-button "Add goal"
          (:help-echo "Options")
        (call-interactively #'canrylog-db-add-goal)
        (revert-buffer))))))

(defun canrylog-view-insert-table (header data)
  "Insert a table.
HEADER should be ((COLUMN . WIDTH) ...).
DATA should be ((VAL VAL2 ...) ...). The number of values per
each member of DATA should be the same as the number of columns
in HEADER."
  (let ((widths (vconcat (mapcar #'cdr header)))
        (col-index 0))
    (cl-loop for (column . width) in header
             do (insert (canrylog--ensure-width width
                          column))
             finally (insert "\n"))
    (dolist (row data)
      (setq col-index 0)
      (dolist (column row)
        (insert
         (canrylog--ensure-width (elt widths col-index)
           (format "%s" column)))
        (cl-incf col-index))
      (insert "\n"))))

(defun canrylog-view--insert-goals (&optional week-offset)
  "Insert goals list.
WEEK-OFFSET specifies the week, 0 is the current one, 1 is last week, and so on."
  (let ((data (->> (canrylog-db-get-goals-list)
                   (--map
                    (let ((realized (canrylog-goal-realized it week-offset))
                          (target (oref it target)))
                      (list
                       (canrylog-view--make-button (oref it name)
                           (:help-echo "Edit this goal"
                            :face 'canrylog-item)
                         (canrylog-view-goal it))
                       (canrylog-format-duration realized)
                       (canrylog-format-duration target)
                       (canrylog-view--percentage (/ realized target 1.0))))))))
    (canrylog-view-insert-table
     `(("Name" . ,(->> data
                       (--map (string-width (car it)))
                       (apply #'max)
                       ;; If it's below 15, use 15 instead
                       (max 15)
                       ;; If it's above 40, use 40 instead
                       (min 40)
                       ;; Except, like, add a few spaces
                       (+ 2)))
       ("Actual" . 10)
       ("Target" . 10)
       ("%" . 6))
     data)))

(canrylog-define-view "goal"
  :keymap (let ((map (make-sparse-keymap)))
            map)
  :major-mode-name "Canrylog-Goal"
  :major-mode-parent canrylog-mode
  :command-arglist (goal)
  :command-body
  ((canrylog-with-ui (:buffer (format "*Canrylog Goal: %s*"
                                      (oref goal name))
                      :display t)
     (canrylog-goal-mode)
     (canrylog--vset goal)
     (revert-buffer)))
  :revert-body
  ((cl-assert (canrylog--vget goal) nil "Missing goal, it's not properly set")
   (canrylog-view-update
    (let* ((goal (canrylog--vget goal))
           (target (oref goal target))
           (realized (canrylog-goal-realized goal)))
      (insert
       (canrylog-widget-title (oref goal name))
       "\n")
      (canrylog-view--insert-button "Rename"
          (:help-echo "Rename this goal"
           :face 'canrylog-button)
        (canrylog-db-rename-goal goal (read-string "New name: " (oref goal name)))
        (revert-buffer))
      (insert
       "\n\n"
       (format "Target: %s"
               (canrylog-view--make-button
                   (canrylog-format-duration target)
                   (:help-echo "Edit target"
                    :face 'canrylog-item)
                 (let ((new (canrylog-read-duration
                             "New target amount (hours): "
                             target)))
                   (canrylog-db-set-goal-target goal new)
                   (revert-buffer))))
       "\n"
       (format "Actual: %s (%s)"
               (canrylog-format-duration realized)
               (canrylog-view--percentage (/ realized target 1.0))))
      (insert
       "\n\n"
       (canrylog-widget-heading "Tasks"))
      (--each (--sort
               (string< (car it) (car other))
               (canrylog-db-get-goal-tasks goal))
        (let ((task (elt it 0))
              (includes-descendants (elt it 1)))
          (insert
           (format
            (if includes-descendants
                "%s"
              "[%s]")
            (canrylog-widget-task-button task))
           "\n  "
           (canrylog-view--make-button "Edit"
               (:face '(canrylog-button))
             (canrylog-db-goal-update-task
              goal task
              (canrylog-read-task "New task for goal: " :initial-input task))
             (revert-buffer))
           " "
           (canrylog-view--make-button "Delete"
               (:face '(canrylog-dangerous canrylog-button))
             (when (y-or-n-p "Delete this task from this goal?")
               (canrylog-db-goal-delete-task goal task)
               (revert-buffer)))
           "\n")))
      (canrylog-view--insert-button "Add task"
          ()
        (canrylog-db-goal-add-task
         goal
         (canrylog-read-task "Add a task to this goal: "))
        (revert-buffer))
      (insert "\n\n")
      (canrylog-view--insert-button "Back to goals list"
          ()
        (canrylog-view-planning))
      (insert "\n\n")
      (canrylog-view--insert-button "Delete this goal"
          (:face '(canrylog-dangerous canrylog-button))
        (when (y-or-n-p "Delete goal definition (tracking data unaffected)? ")
          (canrylog-db-delete-goal goal)
          (kill-buffer)
          (canrylog-view-planning)))))))

;;;;; Evil keybinds

(when (featurep 'evil)
  (evil-define-key* 'normal canrylog-main-mode-map
    (kbd "t") #'canrylog-view-task
    (kbd "l") #'canrylog-view-task-list
    (kbd "R") #'canrylog-view-report
    (kbd "s") #'canrylog-switch-task
    (kbd "v") #'canrylog-visit)

  (evil-define-key* 'normal canrylog-task-mode-map
    (kbd "?") #'canrylog-metadata-dispatch)

  (evil-define-key* 'normal canrylog-task-list-mode-map
    (kbd "s") #'canrylog-view-task-list-change-sorting))

;;;; Provide

(provide 'canrylog-view)

;;; canrylog-view.el ends here
