;;; canrylog-read.el --- Functions to read input from user -*- lexical-binding: t -*-

;;; Commentary:

;; canrylog-read-task: read a task name, with completion for existing tasks
;; canrylog-read-time: read a timestamp

;;; Code:

(require 'dash)
(require 'canrylog-types)
(require 'canrylog-parse)

(declare-function canrylog-db-get-tags-list "canrylog-db")

(defvar canrylog-read-task--history nil "History for `canrylog-read-task'.")
(defvar canrylog-read-new-task--history nil "History for `canrylog-read-new-task'.")
(defvar canrylog-read-time--history nil "History for `canrylog-read-time'.")

;; HACK: I'd want to be able to pass state ("currently viewing this
;; task!") to commands easily without having to do this.
(defvar canrylog-read-task--force-task nil
  "When non-nil, `canrylog-read-task' always returns this.
The function directly calling `canrylog-read-task' should just
use `or' instead of using this mechanism.")

(cl-defun canrylog-read-task
    (&optional (prompt "Task: ") &key (require-match nil) (initial-input nil))
  "Prompt for a task from the user.
PROMPT, INITIAL-INPUT and REQUIRE-MATCH are passed to `completing-read'."
  (or canrylog-read-task--force-task
      ;; I want Ivy sorting to apply to every command that uses this function.
      ;; This allows us to do
      ;;   (setq ivy-prescient-sort-commands '(:not canrylog-read-task))
      ;; to not override sorting for task names.
      (let* ((last-command this-command)
             (this-command 'canrylog-read-task))
        (completing-read prompt
                         (canrylog-get-task-names)
                         nil require-match initial-input
                         'canrylog-read-task--history))))

(cl-defun canrylog-read-tag (&optional (prompt "Tag: "))
  "Prompt for a tag.
PROMPT is passed to `completing-read'."
  (completing-read prompt (-sort #'string< (canrylog-db-get-tags-list))))

(cl-defun canrylog-read-new-task (&optional (prompt "Task: ") initial-input)
  "Prompt for a new task.
PROMPT and INITIAL-INPUT are passed to `read-string'."
  (read-string prompt initial-input 'canrylog-read-new-task--history))

(cl-defun canrylog-read-time (&optional (prompt "Time: "))
  "Prompt for time from user.
PROMPT is passed to `read-string'."
  (read-string prompt (canrylog-iso8601) 'canrylog-read-time--history))

;; TODO: read "00:00:00" instead
(cl-defun canrylog-read-duration (&optional (prompt "Duration (in hours): ")
                                            initial-input-seconds)
  "PROMPT for duration from the user.

If INITIAL-INPUT-SECONDS is provided, convert it to hours then
use it in the prompt.

Ask for hours but return seconds."
  (let* ((initial-input (when (numberp initial-input-seconds)
                          (--> initial-input-seconds
                               ;; To hours
                               (/ it 60 60.0)
                               (format "%.2f" it))))
         (value 0)
         ;; Use `read-string' to get initial-input.
         (input (read-string prompt initial-input)))
    (setq value (read input))
    (unless (numberp value)
      (user-error "Please enter a number"))
    (-> value
        ;; to seconds
        (* 60 60)
        ;; integer number of seconds
        floor)))

(provide 'canrylog-read)

;;; canrylog-read.el ends here
