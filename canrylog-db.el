;;; canrylog-db.el --- Data storage backend -*- lexical-binding: t -*-

;;; Commentary:

;; Helper code for data storage.
;;
;; Right now:
;; - tracking information is stored in tracking.canrylog
;;   (implemented in `canrylog-parse')
;;
;; Using JSON to store data is too ad-hoc when we can just use SQLite.

;;; Code:

(require 'dash)
(require 'sqlite)

(require 'canrylog-vars)

(require 'canrylog-read)

(declare-function xr "xr")

(defvar canrylog-db--connection nil
  "Used to store the DB connection.")
(defvar canrylog-db--in-transaction nil
  "Helper variable for `canrylog-db-with-transaction'.

Functions can read this to know they are in a transaction.")

(defconst canrylog-db--version 6)
(defconst canrylog-db--schemata
  '((tasks
     "task unique primary key"
     "description text")
    (metadata
     "task not null"
     "key not null"
     "value")
    (frequency_goals
     "task references tasks(task)"
     "interval integer not null")
    (tags
     "tag not null"
     "task not null")
    (goals
     ;; INTEGER PRIMARY KEY is alias for ROWID. However, ROWID may change when
     ;; there is no explicit integer primary key.
     ;; https://www.sqlite.org/lang_vacuum.html#how_vacuum_works
     "id integer primary key"
     "name text not null"
     "target integer not null"
     "description text")
    (assoc_goal_task
     "goal_id references goals(id)"
     "task references tasks(task)"
     ;; SQLite has no boolean type. Use 1 for true and NULL for false.
     "descendants integer default 1")))

(defmacro canrylog-db-with-transaction (&rest body)
  "Run BODY in a transaction for `canrylog-db'.
Also sets `canrylog-db--in-transaction'."
  (declare (indent 0))
  `(with-sqlite-transaction (canrylog-db)
     (let ((canrylog-db--in-transaction t))
       ,@body)))

(defun canrylog-db--flatten (rows)
  "Flatten single item ROWS."
  (mapcar #'car rows))

(cl-defun canrylog-db ()
  "Return an open database, initializing if necessary."
  ;; Early return to avoid recursive initialization
  (when canrylog-db--connection
    (cl-return-from canrylog-db canrylog-db--connection))
  (let ((should-init (not (file-exists-p (canrylog-db-file))))
        db)
    (when should-init
      (make-directory canrylog-repo t))
    (setq db (sqlite-open (canrylog-db-file)))
    (setq canrylog-db--connection db)
    (when should-init
      (sqlite-pragma db "foreign_keys = 1")
      (sqlite-pragma db (format "user_version = %s" canrylog-db--version))
      (canrylog-db-with-transaction
        (pcase-dolist (`(,tbl . ,schemata) canrylog-db--schemata)
          (sqlite-execute
           db
           (format "CREATE TABLE \"%s\" (%s);"
                   tbl
                   (string-join schemata ","))))))
    (let ((current-version (caar (sqlite-select db "pragma user_version"))))
      (unless (eql current-version canrylog-db--version)
        (canrylog::message
         "Current version %s is out of date, performing backup..."
         current-version)
        (copy-file (canrylog-db-file)
                   (format "%s.%s.bak"
                           (canrylog-db-file)
                           (format-time-string "%+4Y%m%dT%H%M%S%z"))
                   :ok)
        (catch 'done
          (while t
            (setq current-version (canrylog-db-migrate current-version))
            (when (eql current-version canrylog-db--version)
              (throw 'done t))
            (when (> current-version canrylog-db--version)
              (error "Somehow migrated to a nonexistant version"))))
        (canrylog::message
         "Migration complete")))
    db))
(defun canrylog-db--close ()
  "Close the database connection."
  (setq canrylog-db--connection nil))

;; FIXME: this should be run in a transaction!
(defun canrylog-db-migrate (old &optional new)
  "Migrate from OLD version to NEW version."
  (cl-block nil
    (when (eql old canrylog-db--version)
      (cl-return canrylog-db--version))
    (unless new
      (setq new (1+ old)))
    (pcase (list old new)
      ('(1 2)
       (canrylog-db-execute
        "alter table \"tasks\" drop column \"metadata\"")
       (canrylog-db-execute
        "create table \"metadata\" (task not null, key not null, value)")
       (canrylog-db-execute
        (format "pragma user_version = %s" new)))
      ('(2 3)
       (canrylog-db-execute
        "alter table \"tasks\" add column \"description\" text")
       (canrylog-db-execute
        (format "pragma user_version = %s" new)))
      ('(3 4)
       (canrylog-db-execute
        "alter table \"goals\" rename to \"goals_old\"")
       (canrylog-db-execute
        "create table \"goals\"
(id integer primary key,
 name text not null,
 target integer not null,
 description text)")
       (canrylog-db-execute
        "create table \"assoc_goal_task\"
(goal_id references goals(id),
 task references tasks(task))")
       (--each (canrylog-db-select
                "select name, matcher, target from goals_old")
         (let ((name (elt it 0))
               (matcher (elt it 1))
               (target (elt it 2))
               tasks
               goal-id)
           (canrylog-db-execute
            "insert into goals (name, target) values (?, ?)"
            name target)
           (setq goal-id (canrylog-db-select "select last_insert_rowid()"))
           ;; Translate the matcher to tasks
           (when (featurep 'xr)
             (let ((parsed (xr matcher)))
               (cond
                ;; A plain string
                ((equal parsed matcher)
                 (setq tasks (list matcher)))
                ;; A simple `or' of plain strings
                ((and (listp parsed)
                      (eq (car parsed) 'or)
                      (-all? #'stringp (cdr parsed)))
                 (setq tasks (cdr parsed))))))
           ;; Fallback: stuff matcher into description and tell the user to
           ;; migrate themself
           (unless tasks
             (canrylog-db-execute
              "UPDATE goals SET description = ? WHERE id = ?"
              (concat
               "Was not able to automatically migrate old matcher to new task list. Please re-define the task list manually.\nThe old matcher is:\n"
               matcher)
              goal-id))
           (canrylog-db-insert
            "assoc_goal_task"
            (--map (list goal-id it) tasks))))
       (canrylog-db-execute "drop table goals_old")
       (canrylog-db-execute
        (format "pragma user_version = %s" new)))
      ('(4 5)
       (canrylog-db-execute
        "alter table \"assoc_goal_task\"
add column \"descendants\" integer
default 1")
       (canrylog-db-execute
        (format "pragma user_version = %s" new)))
      ('(5 6)
       (canrylog-db-execute
        "create table \"frequency_goals\"
(task references tasks(task),
 interval integer)")
       (canrylog-db-execute
        (format "pragma user_version = %s" new))))
    new))

(defun canrylog-db-execute (sql &rest args)
  "Run `sqlite-execute' with SQL and ARGS on the cache database.
If SQL is a list, join them with newlines."
  (when (listp sql)
    (setq sql (string-join sql "\n")))
  (let ((db (canrylog-db)))
    (prog1 (sqlite-execute db sql args)
      (unless canrylog-db--in-transaction
        (sqlite-execute db "VACUUM;")))))
(defun canrylog-db-select (sql &rest args)
  "Run `sqlite-select' with SQL and ARGS on the cache database.
If SQL is a list, join them with newlines."
  (when (listp sql)
    (setq sql (string-join sql "\n")))
  (sqlite-select (canrylog-db) sql args))
(defun canrylog-db-insert (table values &optional mode)
  "Insert VALUES into TABLE.
TABLE can be specified as a symbol.
VALUES is a list of rows. Each row is a list of values
for each column.
MODE can be used to override the insertion keyword. By default it
is \"INSERT\", but it can also be \"INSERT OR REPLACE\". No
checks are performed as to whether MODE is valid."
  (when (> (length values) 0)
    (apply
     #'canrylog-db-execute
     ;; The statement is "insert into tbl values (?,?,?), (?,?,?) ..."
     ;; This is constructing the placeholders.
     ;; Then we pass in flattened arguments, and this works.
     (format "%s into \"%s\" values %s"
             (or mode "insert")
             table
             (mapconcat
              (lambda (row)
                (format "(%s)"
                        (-> (make-list (length row) "?")
                            (string-join ","))))
              values
              ","))
     (-flatten values))))

(defun canrylog-db--rename-single-task (old-name new-name)
  "Rename references to a task of OLD-NAME to NEW-NAME.
No transaction is opened. The caller is expected to open a
transaction around a call to this function."
  (canrylog-db-execute "UPDATE metadata SET task = ? WHERE task = ?"
                       new-name old-name)
  (canrylog-db-execute "UPDATE tags SET task = ? WHERE task = ?"
                       new-name old-name)
  (canrylog-db-execute "UPDATE tasks SET task = ? WHERE task = ?"
                       new-name old-name))
(defun canrylog-db-rename-task (old-name new-name &optional keep-children)
  "Rename references to a task of OLD-NAME to NEW-NAME.
If KEEP-CHILDREN is non-nil, also rename its children."
  (canrylog-db-with-transaction
    (canrylog-db--rename-single-task old-name new-name)
    ;; I wish I knew of a better way to do this, perhaps within one query
    ;; but this is the best way I could think of right now.
    (when keep-children
      (let ((descendant-tasks (canrylog-db--flatten
                               (canrylog-db-select
                                "
SELECT task FROM metadata
WHERE task LIKE $1
UNION
SELECT task FROM tags
WHERE task LIKE $1
UNION
SELECT task FROM tasks
WHERE task LIKE $1
"
                                (format "%s:%%" old-name)))))
        (--each descendant-tasks
          (canrylog-db--rename-single-task
           it
           (format "%s:%s"
                   new-name
                   ;; if new-name is "bbbb"
                   ;; and the descendant is "abc:def:ghi"
                   ;; this substring makes it "def:ghi"
                   ;; then appends it back to create "bbbb:def:ghi".
                   (substring it (1+ (length old-name))))))))))

(defun canrylog-db-get-goals-list ()
  "Return all goals."
  (--map
   (canrylog-goal
    :id (elt it 0)
    :name (elt it 1)
    :target (elt it 2)
    :description (elt it 3))
   (canrylog-db-select "SELECT id, name, target, description FROM goals")))

(cl-defun canrylog-db-add-goal (&key name target tasks)
  "Add a goal of TARGET for TASKS with NAME.
TASKS can also be a single task name.
NAME defaults to something derived from TASKS."
  (interactive
   (list :tasks (canrylog-read-task "For this task: ")
         :target (canrylog-read-duration "How many hours per week: ")))
  (setq tasks (ensure-list tasks))
  (unless name
    (setq name (string-join tasks " and ")))
  (canrylog-db-with-transaction
    (let (goal-id)
      (canrylog-db-execute
       "insert into goals (name, target) values (?, ?)"
       name target)
      (setq goal-id (canrylog-db-select "select last_insert_rowid()"))
      (canrylog-db-insert
       "assoc_goal_task"
       (--map (list goal-id it 1) tasks)))))
(defun canrylog-db-delete-goal (goal)
  "Delete a GOAL from the database."
  (let ((id (canrylog-goal-id goal)))
    ;; The assoc table entries will be deleted due to the foreign key
    ;; constraint.
    (canrylog-db-execute "DELETE FROM goals WHERE id = ?" id)))
(defun canrylog-db-set-goal-target (goal new-target)
  "Set the target of GOAL to NEW-TARGET in the database."
  (canrylog-db-execute
   "UPDATE OR REPLACE goals
SET target = ?
WHERE id = ?"
   new-target
   (oref goal id))
  (oset goal target new-target))
(defun canrylog-db-rename-goal (goal new-name)
  "Rename GOAL to NEW-NAME in the database.
GOAL is modified to have NEW-NAME in its name field."
  (canrylog-db-execute
   "UPDATE OR REPLACE goals
SET name = ?
WHERE id = ?"
   new-name
   (oref goal id))
  (oset goal name new-name))
(defun canrylog-db-get-goal-tasks (goal)
  "Get the tasks of GOAL."
  (->> (canrylog-db-select
        "select task, descendants from assoc_goal_task where goal_id = ?"
        (oref goal id))
       (--map
        (list (car it)
              ;; the value is 1 for true, nil for false
              ;; This casts it to t and nil.
              (not (not (cadr it)))))))

(defun canrylog-db-set-frequency-goal (task seconds)
  "Set the target max interval between TASK active instances to be SECONDS.
If SECONDS is nil, unset it."
  (interactive
   (let* ((task (canrylog-read-task "Set frequency goal for task: "))
          (hours (read-number "New target max interval between task activation (in hours): "
                              (-some-> (canrylog-db-get-frequency-goal task)
                                (/ 60 60)))))
     (if (and (integerp hours)
              (> hours 0))
         (list task (* hours 60 60))
       (user-error "Not a valid max interval"))))
  (canrylog-db-execute
   "DELETE FROM frequency_goals
WHERE task = ?"
   task)
  (when seconds
    (canrylog-db-execute
     "INSERT INTO frequency_goals (task,interval) VALUES (?,?)"
     task seconds)))
(defun canrylog-db-get-frequency-goal (task)
  "Get the target interval between activation of TASK in seconds."
  (caar (canrylog-db-select
         "select interval from frequency_goals where task = ?"
         task)))
(defun canrylog-db-list-frequency-goals ()
  "List all tasks who have frequency goals.
Return ((TASK SECONDS) ...)."
  (canrylog-db-select
   "select task, interval from frequency_goals"))

(defun canrylog-db-ensure-task (task &optional description)
  "Ensure a TASK is in the tasks table.
The task hopefully is already in the log file, which is the
source of truth.
The DESCRIPTION is also stored here."
  (canrylog-db-execute
   "insert or ignore into tasks (task, description) values (?,?)"
   task description))

(defun canrylog-db-goal-add-task (goal task)
  "Add TASK to GOAL."
  (canrylog-db-with-transaction
    ;; Ensure the thing is in tasks, for foreign key.
    (canrylog-db-ensure-task task)
    (canrylog-db-execute
     "insert into assoc_goal_task (goal_id, task) values (?, ?)"
     (oref goal id)
     task)))
(defun canrylog-db-goal-delete-task (goal task)
  "Delete TASK from GOAL."
  (canrylog-db-execute
   "DELETE FROM assoc_goal_task WHERE goal_id = ? and task = ?"
   (oref goal id)
   task))
(defun canrylog-db-goal-update-task (goal old-task new-task)
  "Remove OLD-TASK from GOAL then add NEW-TASK."
  (canrylog-db-execute
   "UPDATE assoc_goal_task SET task = ? WHERE goal_id = ? and task = ?"
   new-task
   (oref goal id)
   old-task))

(defun canrylog-db-get-tags-list ()
  "Return a list of all tags."
  (canrylog-db--flatten
   (canrylog-db-select "SELECT DISTINCT tag FROM tags")))

(defun canrylog-db-get-metadata-keys-list ()
  "Return a list of all metadata keys."
  (canrylog-db--flatten
   (canrylog-db-select "SELECT DISTINCT key FROM metadata")))

(defun canrylog-task-metadata (task)
  "Return metadata of TASK in an alist."
  (->> (canrylog-db-select
        "SELECT key, value FROM metadata WHERE task = ?"
        task)
       ;; (("URL" "VALUE") ...) -> ((URL . "VALUE") ...)
       (--map `(,(intern (car it)) . ,(cadr it)))))
(defun canrylog-task-metadata-keys (task-name)
  "Return metadata keys of TASK-NAME."
  (canrylog-db--flatten
   (canrylog-db-select
    "SELECT key FROM metadata WHERE task = ?"
    task-name)))
(defun canrylog-task-set-metadata (task key value)
  "Set metadata KEY to VALUE for TASK.

If VALUE is nil, KEY is deleted as well.

When run interactively, prompt to select from existing keys."
  (interactive
   (let* ((task (or (canrylog--vget task)
                    (canrylog-read-task)))
          (keys (canrylog-task-metadata-keys task)))
     (list task
           (if keys
               (completing-read "Key: " keys)
             (read-string "Key: "))
           (read-string "Value: "))))
  (canrylog-db-with-transaction
    ;; Remove duplicate values
    (canrylog-task-delete-metadata task key)
    (canrylog-db-execute
     "INSERT INTO metadata (task,key,value) VALUES (?,?,?)"
     task key value)))
(defun canrylog-task-delete-metadata (task key)
  "Delete KEY in TASK's metadata."
  (interactive
   (let* ((task (or (canrylog--vget task)
                    (canrylog-read-task)))
          (keys (canrylog-task-metadata-keys task)))
     (unless keys
       (user-error "No keys to delete"))
     (list task (completing-read "Delete key: " keys nil t))))
  (canrylog-db-execute
   "DELETE FROM metadata WHERE task = ? AND key = ?"
   task (format "%s" key)))

(defun canrylog-task-tags (task-name)
  "Return the tags of TASK-NAME."
  (canrylog-db--flatten
   (canrylog-db-select "SELECT tag FROM tags WHERE task = ?" task-name)))
(defun canrylog-tag-tasks (tag)
  "Return tasks that are tagged with TAG."
  (canrylog-db--flatten
   (canrylog-db-select "SELECT DISTINCT task FROM tags WHERE tag = ?" tag)))
(defun canrylog-task-add-tag (task-name tag)
  "Add TAG to the task with TASK-NAME."
  (interactive
   (list (or (canrylog--vget task)
             (canrylog-read-task))
         (canrylog-read-tag "New tag: ")))
  (canrylog-db-insert 'tags (list (list tag task-name))))
(defun canrylog-task-delete-tag (task-name tag)
  "Delete TAG from the task with TASK-NAME."
  (interactive
   (let* ((task (or (canrylog--vget task)
                    (canrylog-read-task)))
          (tags (canrylog-task-tags task)))
     (unless tags
       (user-error "No tags to delete"))
     (list task (completing-read "Delete tag: " tags))))
  (canrylog-db-execute
   "DELETE FROM tags WHERE task = ? AND tag = ?"
   task-name tag))

(defun canrylog-task-description (task-name)
  "Return the description for TASK-NAME."
  (caar
   (canrylog-db-select "SELECT description FROM tasks WHERE task = ?" task-name)))
(defun canrylog-task-set-description (task-name new)
  "Set the description of TASK-NAME to NEW.
NEW is trimmed before being set. If NEW is empty, remove the description."
  (when (stringp new)
    (setq new (string-trim new)))
  ;; Try init & set at once (if the row doesn't exist)
  (setq init-rows (canrylog-db-ensure-task task-name new))
  ;; If we did not init, we have existing row(s) to update
  (when (= 0 init-rows)
    (canrylog-db-execute
     "UPDATE tasks
SET description = ? WHERE task = ?"
     ;; Empty string after trim = set to nil
     (unless (string-empty-p new)
       new)
     task-name)))

(provide 'canrylog-db)

;;; canrylog-db.el ends here
