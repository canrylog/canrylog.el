;;; canrylog-buffers.el --- Buffer management -*- lexical-binding: t -*-

;;; Commentary:

;; Buffer management code for Canrylog.

;;; Code:

(require 'canrylog-db)

(defvar canrylog-buffer-name "*Canrylog*")

(defcustom canrylog-max-buffers 5
  "The maximum number of buffers when `canrylog-navigation-style' is `multiple'.

nil means no maximum."
  :group 'canrylog
  :type '(choice (const :tag "No maximum" nil)
                 (integer :tag "Max number of buffers")))

(defvar canrylog--buffers nil
  "A list to keep track of Canrylog buffers.")

(defun canrylog-buffers--cleanup ()
  "Clean up Canrylog buffers to honor `canrylog-max-buffers'."
  (setq canrylog--buffers
        (-filter #'buffer-live-p canrylog--buffers))
  (when (and canrylog-max-buffers
             (> (length canrylog--buffers) canrylog-max-buffers))
    (--each (nthcdr canrylog-max-buffers canrylog--buffers)
      ;; Only kill when buffer is not displayed.
      (unless (get-buffer-window it)
        (kill-buffer it))))
  (setq canrylog--buffers
        (-filter #'buffer-live-p canrylog--buffers)))

(defun canrylog-buffers--push (buf)
  "Push BUF to the top of `canrylog--buffers'.
Also clean up old buffers if necessary."
  ;; Ensure the buffer is on top
  (setq canrylog--buffers (remove buf canrylog--buffers))
  (push buf canrylog--buffers)
  (when (numberp canrylog-max-buffers)
    (canrylog-buffers--cleanup)))

(defun canrylog-kill-buffers ()
  "Kill all Canrylog buffers."
  (interactive)
  (mapc #'kill-buffer canrylog--buffers)
  (setq canrylog--buffers nil)
  (canrylog-db--close))

(provide 'canrylog-buffers)

;;; canrylog-buffers.el ends here
