#!/usr/bin/env python3
import matplotlib.pyplot as plt
import json
import sys

data = json.load(sys.stdin)

# plt.style.use("dark_background")

fig, ax = plt.subplots()
fig.set_size_inches(4, 4)
ax.pie(data.values(), radius=1.5, startangle=90, counterclock=False)
fig.savefig(sys.stdout.buffer, dpi=100, transparent=True)
