;;; canrylog-export.el --- Data export and integration  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(require 'cl-lib)
(require 'canrylog-read)
(require 'canrylog-utils)
(require 'canrylog-methods)

(defun canrylog-export--task-duration-alist--weekly (task &optional range)
  "Calculate duration of TASK for each week."
  (let ((tbl (make-hash-table :test 'equal))
        (earliest-moment nil)
        (latest-moment nil)
        alist)
    (canrylog-parse-for-each-event-pair task
      :descendants t
      :range range
      :fn (lambda (_this-task start end)
            ;; Update earliest and latest moments for later use
            (when (or (not earliest-moment)
                      (time-less-p start earliest-moment))
              (setq earliest-moment start))
            (when (or (not latest-moment)
                      (time-less-p latest-moment end))
              (setq latest-moment end))
            ;; Count duration within each day between START and END
            (dolist (interval (canrylog-week-intervals-between-moments start end))
              (cl-incf
               (gethash (oref interval start) tbl 0)
               (- (canrylog-time-clamp end interval)
                  (canrylog-time-clamp start interval))))))
    ;; We still do not have datapoints for days where there were nothing
    ;; logged. We want to have datapoints that are 0 in this case to represent
    ;; that we know it's 0 instead of being uncertain.
    ;;
    ;; This massively inflates the number of points though.
    ;;
    (--each (canrylog-week-intervals-between-moments earliest-moment latest-moment)
      (unless (gethash (oref it start) tbl)
        (puthash (oref it start) 0 tbl)))
    ;; Now we have the hash table with everything we want. Convert it to an
    ;; alist so we can better sort it.
    (maphash (lambda (k v) (push (cons k v) alist)) tbl)
    (--sort (time-less-p (car it) (car other))
            alist)))

(defun canrylog-export--task-duration-alist (task &optional range)
  "Calculate duration of TASK for each day.
Return an alist mapping days (as time values) to durations (in seconds)."
  (let ((days-table (make-hash-table :test 'equal))
        ;; (earliest-moment nil)
        ;; (latest-moment nil)
        alist)
    (canrylog-parse-for-each-event-pair task
      :descendants t
      :range range
      :fn (lambda (_this-task start end)
            ;; Update earliest and latest moments for later use
            ;; (when (or (not earliest-moment)
            ;;           (time-less-p start earliest-moment))
            ;;   (setq earliest-moment start))
            ;; (when (or (not latest-moment)
            ;;           (time-less-p latest-moment end))
            ;;   (setq latest-moment end))
            ;; Count duration within each day between START and END
            (dolist (day-interval (canrylog-day-intervals-between-moments start end))
              (cl-incf
               (gethash (oref day-interval start) days-table 0)
               (- (canrylog-time-clamp end day-interval)
                  (canrylog-time-clamp start day-interval))))))
    ;; We still do not have datapoints for days where there were nothing
    ;; logged. We want to have datapoints that are 0 in this case to represent
    ;; that we know it's 0 instead of being uncertain.
    ;;
    ;; This massively inflates the number of points though.
    ;;
    ;; (--each (canrylog-day-intervals-between-moments earliest-moment latest-moment)
    ;;   (unless (gethash (oref it start) days-table)
    ;;     (puthash (oref it start) 0 days-table)))
    ;; Now we have the hash table with everything we want. Convert it to an
    ;; alist so we can better sort it.
    (maphash (lambda (k v) (push (cons k v) alist)) days-table)
    (--sort (time-less-p (car it) (car other))
            alist)))

(defun canrylog-export--one-time-server (str)
  "Start a server that serves STR once then shuts down.
Return the URL to the server. The server shut down happens later."
  (let ((server nil)
        (port (+ 10000 (random 10000))))
    (setq server (make-network-process
                  :name "canrylog"
                  :service port
                  :server t
                  :buffer nil
                  :filter (lambda (proc _str)
                            ;; This is the bare minimum to tell browsers this
                            ;; can load over another web page.
                            (process-send-string proc "HTTP/1.1 200 OK\r\n")
                            (process-send-string proc "Access-Control-Allow-Origin: *\r\n")
                            (process-send-string proc "\r\n")
                            (process-send-string proc str)
                            (delete-process proc)
                            ;; self destruct
                            (delete-process server))
                  :host 'local
                  :family 'ipv4))
    (format "http://localhost:%s" port)))

(cl-defun canrylog-export--xmrit (data &key x-label y-label)
  "Show DATA on Xmrit.

X-LABEL and Y-LABEL are passed to Xmrit if provided.

DATA should be an alist mapping dates to values.

Xmrit is local-only. DATA is sent to the browser via a server from Emacs
that closes once a response is sent, and never leaves this computer."
  (let* ((one-time-url
          (canrylog-export--one-time-server
           (json-encode
            `(,@(when x-label `((xLabel . ,x-label)))
              ,@(when y-label `((yLabel . ,y-label)))
              (xdata . ,(--map
                         `((x . ,(car it))
                           (value . ,(cdr it)))
                         data)))))))
    (browse-url (format "https://xmrit.com/t/#v=1&d=%s" one-time-url))))

(cl-defun canrylog-export-task-duration-xmrit (task &key type)
  "Export TASK\\='s durations to be shown in Xmrit.

TYPE is `daily' or `weekly'.

Xmrit is local-only. DATA is sent to the browser via a server from Emacs
that closes once a response is sent, and never leaves this computer."
  (interactive (list (canrylog-read-task "Show daily duration graph for task: ")))
  (canrylog-export--xmrit
   (->>
    (if (eq type 'daily)
        (canrylog-export--task-duration-alist task)
      (canrylog-export--task-duration-alist--weekly task))
    (--map
     (cons (format-time-string "%F" (car it))
           ;; We don't average this out for weekly view here because I found it
           ;; to be a bit confusing, since I already expect a weekly number, and
           ;; I keep multiplying it back.
           (format "%.2f" (/ (cdr it) 3600.0)))))
   :x-label task
   :y-label (if (eq type 'daily)
                "Hours"
              "Hours per week")))

(cl-defun canrylog-export-task-duration (task &key file format (hour t))
  "Export the duration of TASK into FILE for each day as CSV.

If HOUR is non-nil, write out hours (default), otherwise write out seconds.

FORMAT can be `tsv' or `csv'. If nil, `tsv' is default.

If FILE is a directory, export to <dir>/<task base name>.csv. For
example, if FILE is \"/tmp/\" and the task is called
\"Work:ProjectA\", the task is exported as \"/tmp/ProjectA.csv\"."
  (interactive
   (list (canrylog-read-task)
         :file (read-file-name "Export to: " nil "export.csv")))
  (setq task (canrylog::ensure-task-name task))
  (unless format
    (setq format 'tsv))
  (when (file-directory-p file)
    (setq file (expand-file-name (format "%s.%s"
                                         (canrylog::task-name:base task)
                                         (if (eq format 'csv) "csv" "tsv"))
                                 file)))
  (let ((alist (canrylog-export--task-duration-alist task))
        (separator (if (eq format 'csv)
                       ","
                     "\t")))
    (with-temp-file file
      (insert (format "%s\n" (s-join separator '("date" "duration"))))
      (cl-loop for (day . duration) in alist
               do
               (insert
                (format
                 "%s\n"
                 (s-join separator
                         (list (format-time-string "%F" day)
                               (format "%s" (if hour
                                                (/ duration 3600.0)
                                              duration)))))))))
  (message "Successfully exported to %s." file))

(provide 'canrylog-export)
;;; canrylog-export.el ends here
