;;; canrylog-cache.el --- Saving results  -*- lexical-binding: t; -*-
;;; Commentary:

;; Saving results to a common cache.

;;; Code:

(require 'subr-x)
(require 'cl-lib)

(defvar canrylog-cache nil
  "Cache for various Canrylog functions.")

(defvar canrylog-cache--enabled t
  "Whether the cache is enabled.")

(defvar canrylog-cache-invalid nil
  "List of which caches are invalid.

A cache is a sublist in `canrylog-cache'.

For example, in

\((events . ((canrylog-event ...)
            (canrylog-event ...)
            ...))
 (tasks . ((canrylog-task ...)
           ...))

, `events' and `tasks' are caches.")

(defun canrylog-cache-invalidate (&optional cache)
  "Invalidate the Canrylog cache.

If CACHE is non-nil, clear the value of CACHE in
`canrylog-cache'. Otherwise clear everything in `canrylog-cache'.

Functions accessing CACHE will update it next time they use it."
  (interactive)
  (if cache
      (push cache canrylog-cache-invalid)
    (setq canrylog-cache nil)))

(defun canrylog-cache (cache update &optional func &rest update-args)
  "Run FUNC which receives a stored value in CACHE as an argument.

Return the value that FUNC returns.

FUNC is `identity' by default.

When CACHE is invalid (either nil or if CACHE is in
`canrylog-cache-invalid'), populate CACHE with function UPDATE,
then remove CACHE from `canrylog-cache-invalid'. UPDATE-ARGS are
passed to it as well.

UPDATE receives one argument, the current value of CACHE."
  (declare (indent 1))
  (unless func
    (setq func #'identity))
  ;; If disabled, just bypass the cache
  (if (not canrylog-cache--enabled)
      (funcall func (apply update update-args))
    ;; If cache is valid
    (if-let ((cached (and
                      canrylog-cache--enabled
                      (not (member cache canrylog-cache-invalid))
                      (cdr (assoc cache canrylog-cache)))))
        ;; Then just pass the value to FUNC
        (funcall func cached)
      ;; Otherwise run UPDATE to get the value, store it in cache, then pass it
      ;; to FUNC
      (let ((canrylog-cache--enabled t))
        ;; update cache
        (let ((new (apply update update-args)))
          (if (assoc cache canrylog-cache)
              (setf (cdr (assoc cache canrylog-cache)) new)
            (push (cons cache new) canrylog-cache))
          ;; now it's valid
          (setq canrylog-cache-invalid (delete cache canrylog-cache-invalid))
          (funcall func new))))))

(defun canrylog-cache-table (cache update id &optional direct-id)
  "Hash-table-based cache.
CACHE: the name of the cache, as a symbol.
UPDATE: the function to call to get the new value.
ID is used to generate the key to the hash table. If nil, bypass the cache.
If DIRECT-ID is non-nil, ID is used directly as the key, instead
of generating a hash."
  (declare (indent 1))
  (canrylog-cache cache
    (lambda ()
      (make-hash-table :test #'equal))
    (lambda (table)
      (cl-block nil
        (unless id
          (cl-return (funcall update)))
        (cl-with-gensyms (notfound)
          (let* ((key (if direct-id
                          id
                        (sha1 (format "%s" id))))
                 (value (gethash key table notfound)))
            (when (eq value notfound)
              (setq value (funcall update))
              (puthash key value table))
            value))))))

(provide 'canrylog-cache)

;;; canrylog-cache.el ends here
