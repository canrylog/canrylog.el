;;; canrylog-utils.el --- Utility functions  -*- lexical-binding: t; -*-
;;; Commentary:

;; Library and language extensions used by Canrylog.

;;; Code:

(require 'map)
(require 'dash)
(require 's)

(require 'canrylog-vars)

(declare-function org-read-date "org")
(declare-function calendar-read-date "calendar")
(defvar org-read-date-prefer-future)

(require 'shr)

(defun canrylog--truncate-string (len str &optional ellipsis)
  "Truncate STR to LEN, adding ELLIPSIS (default `...').

Like `s-truncate', except we use the display
width (`string-width') to determine whether to truncate."
  (unless ellipsis
    (setq ellipsis "..."))
  (setq len (floor len))
  (if (<= (string-width str) len)
      str
    (condition-case nil
        (format "%s%s"
                (substring str 0 (- len (string-width ellipsis)))
                ellipsis)
      ;; There is no version of `substring' that is based on the
      ;; display width, so this is the hack around it. We only run
      ;; this if `substring' barfs.
      (args-out-of-range
       (cl-loop until (<= (string-width str)
                          (- len (string-width ellipsis)))
                do (setq str (substring str 0 (1- (length str))))
                finally return (format "%s%s" str ellipsis))))))

(defun canrylog--ensure-width (len str &optional ellipsis)
  "Truncate or lengthen STR to LEN (with ELLIPSIS)."
  (declare (indent 1))
  (setq len (floor len))
  (concat (canrylog--truncate-string len str ellipsis)
          (make-string
           (max 0 (- len (string-width str)))
           ?\s)))

(defun canrylog-read-date (prompt)
  "If `org-read-date' is available, read a date using it.

Pass PROMPT to `org-read-date'."
  (cond
   ((fboundp #'org-read-date)
    (let ((org-read-date-prefer-future nil))
      (org-read-date nil nil nil prompt)))
   (t (-let (((m d y) (calendar-read-date)))
        (format "%s-%s-%s" y m d)))))

(defun canrylog-regexp-task (task &optional descendants)
  "Return a regexp that matches an event switching to TASK exactly.
If DESCENDANTS is non-nil, return one that includes the descendants instead."
  (if descendants
      (rx bol
          (one-or-more (not space))
          "  "
          (or (literal task)
              (seq (literal task) ":" (* any)))
          eol)
    (format "^[^[:space:]]+  %s$" (regexp-quote task))))

(defmacro canrylog-with-file (save &rest body)
  "Run BODY with the tracking file opened as the current buffer.

When SAVE, save the buffer back into the tracking file.
`canrylog-after-save-hook' is run afterwards, with
variable `buffer-file-name' set to the tracking file."
  (declare (indent 1))
  `(save-excursion
     (let ((find-file-suppress-same-file-warnings t))
       (with-current-buffer (find-file-noselect (canrylog-file))
         ,(if save
              `(prog1 (progn ,@body)
                 (run-hooks 'canrylog-before-save-hook)
                 (basic-save-buffer)
                 ;; just modified the file, refresh the cache on next use
                 (canrylog-cache-invalidate)
                 (run-hooks 'canrylog-after-save-hook))
            `(progn ,@body))))))

(defun canrylog--pop-to-buffer (buffer)
  "Pop to BUFFER.
If we're in a `canrylog-mode' buffer, use
`pop-to-buffer-same-window', otherwise use `pop-to-buffer'."
  (if (derived-mode-p 'canrylog-mode)
      (pop-to-buffer-same-window buffer)
    (pop-to-buffer buffer)))

;; Or should I only capitalize lower-case only strings?
;; Is there a use case for mixed upper-case? (camelCase?)
(defun canrylog-capitalize (string)
  "Capitalize STRING, but keep all caps intact."
  (let ((case-fold-search nil))
    (if (string-match-p "^[[:upper:]]*$" string)
        string
      (capitalize string))))

(defsubst canrylog-keyword-to-symbol (kw)
  "Given keyword :KW, return 'KW.

Return KW unchanged if it's not a keyword."
  (if (not (keywordp kw))
      kw
    (intern (substring (symbol-name kw) 1))))

(defun canrylog-duplicated (list &optional testfn)
  "Return elements in LIST that are duplicated according to TESTFN."
  (let (numbers)
    ;; count how many times an element appears
    (mapc (lambda (elt)
            (cl-incf (alist-get elt numbers 0 nil testfn)))
          list)
    (map-keys (--filter (> (cdr it) 1) numbers))))

(cl-defun canrylog--insert-html (html &key (pre t))
  "Insert HTML as rendered by shr.

If PRE is non-nil (default), wrap the HTML in <pre> tags to make
sure no `variable-pitch' face is added."
  (require 'shr)
  (require 'xml)
  (shr-insert-document
   (with-temp-buffer
     (insert (format (if pre "<pre>%s</pre>" "%s") html))
     (libxml-parse-html-region (point-min) (point-max))))
  ;; `shr-insert-document' seems to insert a newline at the end. Get
  ;; rid of it.
  (when (= (char-before) (string-to-char "\n"))
    (delete-char -1)))

(defun canrylog--delete-line ()
  "Delete current line."
  (let ((start (line-beginning-position))
        (end (line-end-position)))
    (unless (eql end (point-max))
      (setq end (1+ end)))
    (delete-region start end)))

(defun canrylog--today (&optional n)
  "Return today's date, taking `org-extend-today-until' into account.

Return values look like \"2020-01-23\".

If N is non-nil, return N days from today. For example, N = 1
means tomorrow, and N = -1 means yesterday."
  (unless n
    (setq n 0))
  (format-time-string
   "%Y-%m-%d"
   (time-add
    (* n 86400)
    (time-since
     ;; if it's bound and it's a number, do the same thing `org-today' does
     (or (and (boundp 'org-extend-today-until)
              (numberp org-extend-today-until)
              (* 3600 org-extend-today-until))
         ;; otherwise just return (now - 0) = now.
         0)))))

;; Making up for `days-between''s lack of support for YYYY-MM-DD.
(defun canrylog--parse-date (date)
  "Parse DATE into an encoded time value.

DATE should be a string in the format \"YYYY-MM-DD\"."
  (save-match-data
    (when (string-match "\\(....\\)-\\(..\\)-\\(..\\)" date)
      (encode-time 0 0 0
                   (string-to-number (match-string 3 date))
                   (string-to-number (match-string 2 date))
                   (string-to-number (match-string 1 date))))))

;; This name comes from `days-between' from time-date.el.
(defun canrylog--days-between (date1 date2)
  "Subtract DATE1 from DATE2.

Both of them should be ISO 8601 dates, in the format \"YYYY-MM-DD\"."
  (- (time-to-days (canrylog--parse-date date1))
     (time-to-days (canrylog--parse-date date2))))

(defun canrylog::warn (fmt &rest args)
  "Display a warning (in *Warnings*) for Canrylog.
FMT and ARGS are formatted by `format-message'."
  (apply #'lwarn '(canrylog) :warning fmt args))

(defun canrylog::message (fmt &rest args)
  "Pass FMT and ARGS to `message' in a consistent way."
  (apply #'message (concat "(canrylog) " fmt) args)
  (redisplay))

(defmacro canrylog::loading (str &rest body)
  "Show STR, run BODY, then show STR + \"done\"."
  (declare (indent 1))
  `(progn
     (canrylog::message "%s" ,str)
     ,@body
     (canrylog::message "%sdone" ,str)))

(defun canrylog::face (face &rest strings)
  "Attach FACE to STRINGS.
STRINGS are concatenated together."
  (declare (indent 1))
  (propertize (string-join strings) 'face face))

(defun canrylog::python-plot (type input)
  "Make a plot of TYPE from INPUT, using Python script files.
The plot is returned as raw PNG data as bytes in a unibyte string.
If not in a graphical display, return nil."
  (declare (indent 1))
  (when (display-graphic-p)
    (let ((status nil))
      (with-temp-buffer
        (set-buffer-multibyte nil)
        (setq status (call-process-region input nil "python" nil t nil
                                          (expand-file-name
                                           (format "%s.py" type)
                                           ;; If not `load-file-name'
                                           canrylog--base-dir)))
        (when (equal status 0)
          (buffer-substring-no-properties (point-min) (point-max)))))))

(provide 'canrylog-utils)

;;; canrylog-utils.el ends here
