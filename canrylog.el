;;; canrylog.el --- Personal time tracking  -*- lexical-binding: t; -*-

;; Authors: Kisaragi Hiu <mail@kisaragi-hiu.com>
;; URL: https://gitlab.com/canrylog/canrylog.el
;; Version: 0.12.0
;; Package-Requires: ((emacs "29.1") (s "1.2.0") (dash "2.18.1") (transient "0.4.0"))
;; Keywords: convenience productivity

;; This file is NOT part of GNU Emacs.

;; canrylog.el is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; canrylog.el is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with canrylog.el
;; If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; My take on time tracking.
;; The most recent task is the current task, and only the start of a task is recorded.

;; file format:
;; ```
;; <timestamp>  <task>
;; ...
;; ```
;; with two spaces seperating the timestamp and the task.

;; TODO: reports: stats about a task, stats about a period (what did I
;; do that day?)

;;; Code:

;;;; Requires
(require 'cl-lib)
(require 'dash)
(require 'parse-time)
(require 's)
(require 'seq)
(require 'pcase)
(require 'transient)

(require 'subr-x)
(require 'calendar)

(require 'canrylog-utils)
(require 'canrylog-types)
(require 'canrylog-parse)
(require 'canrylog-file)
(require 'canrylog-read)

;;;; User option
(require 'canrylog-vars)

(require 'canrylog-commands)

(require 'canrylog-view)

;;;###autoload
(defalias 'canrylog #'canrylog-view-main "Open the Canrylog dashboard.")

;;;###autoload (autoload 'canrylog-dispatch "canrylog" nil t)
(transient-define-prefix canrylog-dispatch ()
  "Invoke a Canrylog command from a list of available commands."
  ;; See (transient)Predicate Slots
  ["Log"
   [("p" "prompt for time"  "prompt")
    ("s" "Switch task"      canrylog-switch-task)
    ("o" "Clock out"        canrylog-clock-out)]]
  [["Views"
    ("r" "Dashboard"        canrylog-view-main)
    ("t" "Describe a task"  canrylog-view-task)
    ("T" "Describe a tag"   canrylog-view-tag)
    ("l" "Task list"        canrylog-view-task-list)
    ("g" "Goals"            canrylog-view-planning)
    ("v" "Visit log file"   canrylog-visit)]
   ["Reports"
    ("d" "Daily"    (lambda ()
                      (interactive)
                      (canrylog-view-report 'day 0)))
    ("w" "Weekly"   (lambda ()
                      (interactive)
                      (canrylog-view-report 'week 0)))
    ("m" "Monthly"  (lambda ()
                      (interactive)
                      (canrylog-view-report 'month 0)))
    ("e" "Export"   canrylog-view-export)
    ("x" "Duration XmR" canrylog-export-task-duration-xmrit)]])

;;;; Provide

(provide 'canrylog)

;;; canrylog.el ends here
