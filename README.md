# canrylog.el
[![License: GPL v3+](https://img.shields.io/badge/License-GPL%20v3+-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Coverage Status](https://coveralls.io/repos/gitlab/canrylog/canrylog.el/badge.svg?branch=master)](https://coveralls.io/gitlab/canrylog/canrylog.el?branch=master)

Personal time tracking.

*Currently in experimental stage, developed with the assumption that I'm the only user.*

<img src="images/0.11.0-main-clocked-in.png" width=200>
<img src="images/0.11.0-tag.png" width=200>
<img src="images/0.11.0-daily.png" width=200>

## Install

### [`straight.el`](https://github.com/raxod502/straight.el)

```elisp
(straight-use-package '(canrylog :type git :host gitlab :repo "canrylog/canrylog.el"))
```

## Mental model

Instead of clocking in and out of tasks, Canrylog treats the clocked out state as a task as well. Just use `canrylog-switch-task` to switch to a different task (or use the UI with `M-x canrylog RET`).

There is no special support for multi-tasking, though nothing is stopping one from switching to one task that actually represents several.

### Manually

Put `canrylog.el` under your `load-path`, then `(require 'canrylog)` in your init file.

## Usage

- `canrylog`: Open the dashboard.
- `canrylog-dispatch`: Open a Transient popup of commands.
- `canrylog-switch-task`: Switch to another task.

## Customization

- `canrylog-repo`: directory to store data in; defaults to `~/.canrylog`
- `canrylog-before-switch-task-hook`: Hook run before switching tasks.
- `canrylog-after-switch-task-hook`: Hook run after switching tasks.
- `canrylog-after-save-hook`: Hook run after saving `canrylog-file`.
- `canrylog-before-save-hook`: Hook run after making changes but before saving them to the tracking file
- `canrylog-downtime-task`: When this task is active, the home screen says “Clocked out” and does not keep ticking its clock.

## File format
Events are stored in this file as individual lines, each having a timestamp and a task. Example:

```
1546693919  programming:canrylog.el
1546695379  programming:cangjie.el
1546696800  sleep
```

The timestamp is a unix timestamp, ie. seconds since epoch. They can also be full ISO 8601 timestamps (like "2023-12-01T00:00:00+0900") - which timestamp format to write is determined by the user option `canrylog-timestamp-format`.

Task metadata is stored in a separate SQLite database, utilizing the builtin SQLite integration added in Emacs 29.

# Other solutions

<https://www.emacswiki.org/emacs/CategoryTimeTracking>

- [logtime](https://github.com/bevesce/logtime): plain text
- `timeclock.el`: <kbd>C-h f</kbd> `timeclock-in` <kbd>RET</kbd>
- Org mode
- [WakaTime](https://wakatime.com/), `wakatime-mode` on MELPA
- [ActivityWatch](https://activitywatch.net), `activity-watch-mode` on MELPA
- [harvestime](https://github.com/jgdavey/harvestime): plain text as well, centered around vim
