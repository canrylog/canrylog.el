;;; canrylog-ifn.el --- interval-from-name -*- lexical-binding: t -*-

;;; Commentary:

;; The interval-from-name function is getting too big, so here's a module for
;; it.

;;; Code:

(defvar org-extend-today-until)

(require 'calendar)

(require 'canrylog-types)
(require 'canrylog-utils)

(defun canrylog-ifn--day (offset &optional moment)
  "Return an interval for the day containing MOMENT, offset by OFFSET.

OFFSET of nil or 0 is today, 1 is tomorrow, -1 is yesterday, and
so on.

The caller is responsible for coercing MOMENT to be usable
directly by `format-time-string'."
  (when (bound-and-true-p org-extend-today-until)
    (setq moment (time-subtract moment (* 3600 org-extend-today-until))))
  (when offset
    (setq moment (time-subtract moment (- (* 86400 offset)))))
  (canrylog-interval
   :start (canrylog-time-coerce
           (format-time-string "%FT00:00:00%z" moment))
   :end (canrylog-time-coerce
         (format-time-string "%FT23:59:59%z" moment))))
(defun canrylog-ifn--week (offset &optional moment)
  "Return an interval for the week containing MOMENT.
OFFSET of nil or 0 means this week, -1 means last week, 1 means
next week, and so on.

The caller is responsible for coercing MOMENT to be usable
directly by `format-time-string'."
  ;; support `org-extend-today-until' here as we're not using --today here.
  ;; If o-e-t-u is 1 and it's Monday 0:30, that should be considered as
  ;; Sunday, so we can just knock 0:30 back by 1 hour.
  (when (bound-and-true-p org-extend-today-until)
    (setq moment (time-subtract moment (* 3600 org-extend-today-until))))
  ;; handle week offset by knocking moment back in time
  (when offset
    (setq moment (time-subtract moment (- (* 86400 7 offset)))))
  ;; Current weekday relative to `calendar-week-start-day'. So weekday = 1
  ;; (0-index) is "second day of the week", ie. Monday if the start day is
  ;; Sunday, or Wednesday if the start day is Tuesday.
  (let ((weekday (mod (- (string-to-number (format-time-string "%w" moment))
                         calendar-week-start-day)
                      7)))
    (canrylog-interval :start
                       (canrylog-time-coerce
                        (format-time-string
                         "%FT00:00:00%z"
                         (time-subtract
                          moment
                          (* 60 60 24
                             weekday))))
                       :end
                       (canrylog-time-coerce
                        (format-time-string
                         "%FT23:59:59%z"
                         (time-add
                          moment
                          (* 60 60 24
                             (- 6 weekday))))))))
(defun canrylog-ifn--month (offset &optional moment)
  "Return an interval for the month containing MOMENT.
OFFSET of nil or 0 is this month, 1 is next month, -1 is last month, and so on.
MOMENT is assumed to be encoded time."
  (when (bound-and-true-p org-extend-today-until)
    (setq moment (time-subtract moment (* 3600 org-extend-today-until))))
  (pcase-let
      ((`(,_ ,_ ,_ ,_ ,month ,year)
        (decode-time moment)))
    (when offset
      (calendar-increment-month month year offset))
    (canrylog-interval
     ;; "%+4Y" is added in Emacs 27: See NEWS.27#L2661
     :start
     (canrylog-time-coerce
      (format
       (format-time-string
        "%%04d-%%02d-01T00:00:00%z"
        moment)
       year month))
     :end
     (canrylog-time-coerce
      (format
       (format-time-string
        "%%04d-%%02d-%%sT23:59:59%z"
        moment)
       year month (calendar-last-day-of-month month year))))))

(defun canrylog-interval-from-name (name &optional arg)
  "Return an interval described by NAME (a symbol).

ARG\\='s meaning depends on NAME.

NAME can be:

- `day' (or `today'): 00:00:00 to 23:59:59 of today, taking
  `org-extend-today-until' into account if it is bound. ARG
  defines the offset from today: 0 is today, 1 is tomorrow, -1 is
  yesterday, and so on.
- `week' (or `this-week'): 00:00:00 of beginning of the
  week (depending on `calendar-week-start-day') to 23:59:59 of
  end of the week. ARG is the week offset: 0 or nil is this week,
  1 is next week, -1 is last week, and so on.
- `month' (or `this-month'): 00:00:00 of the first day of this
  month to 23:59:59 of the last day of this month. ARG is the
  offset: 0 is this month, 1 is next month, -1 is last month, and
  so on.
- `day-containing': the day containing ARG as a moment. ARG can
  also be a string with a length of 10 like 2020-03-15.
- `week-containing': the week containing ARG as a moment, taking
  `calendar-week-start-day' into account.
- `month-containing': the month containing ARG as a moment.

The \"containing\" modes do not honor `org-extend-today-until' as that
requires more consideration for the timestamps returned."
  (let ((moment (current-time)))
    (pcase name
      (`day-containing
       (let ((org-extend-today-until 0))
         (canrylog-ifn--day
          0
          (if (and (stringp arg) (= 10 (length arg)))
              (-> (format-time-string "%%sT%%02d:00:00%z") ; grab the current timezone
                  (format arg (or org-extend-today-until 0))
                  canrylog-time-coerce)
            (canrylog-time-coerce arg)))))
      (`week-containing
       (let ((org-extend-today-until 0))
         (canrylog-ifn--week 0 (canrylog-time-coerce arg))))
      (`month-containing
       (let ((org-extend-today-until 0))
         (canrylog-ifn--month 0 (canrylog-time-coerce arg))))
      ((or `day `today) (canrylog-ifn--day arg moment))
      ((or `week `this-week) (canrylog-ifn--week arg moment))
      ((or `month `this-month) (canrylog-ifn--month arg moment)))))

(provide 'canrylog-ifn)

;;; canrylog-ifn.el ends here
