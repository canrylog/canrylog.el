.PHONY: test

test: .cask
	cask emacs --batch -L . -l test/canrylog-test.el -f ert-run-tests-batch

.cask: Cask
	cask install
	touch .cask
