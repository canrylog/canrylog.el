;;; canrylog-types.el --- types, structs, and methods  -*- lexical-binding: t; -*-
;;; Commentary:

;; Types and methods.

;;; Code:
(require 's)
(require 'dash)
(require 'parse-time)
(require 'calendar)

(require 'canrylog-vars)
(require 'canrylog-utils)
(require 'canrylog-cache)

(require 'eieio)

;;;; Generic
(cl-defgeneric canrylog-serialize (thing)
  "Convert THING to a string."
  (format "%s" thing))

(cl-defstruct (canrylog-event (:copier nil)
                              (:constructor canrylog-event))
  moment task)
(defclass canrylog-interval ()
  ((start :initarg :start)
   (end :initarg :end)
   (task :initarg :task))
  :documentation "Represents an interval between time values.")

;;;; Timestamp type
(defun canrylog--encoded-time-p (thing)
  "Return whether THING is usable as an encoded time value."
  (when thing
    (cond ((numberp thing)
           (> thing 0))
          ((listp thing)
           (and (memq (length thing) '(2 4))
                (--all? (integerp it) thing)))
          ((consp thing)
           (and (integerp (car thing))
                (integerp (cdr thing)))))))

(defun canrylog-iso8601 (&optional time zone)
  "Return a full ISO 8601 timestamp at TIME in ZONE."
  (format-time-string "%FT%T%z" time zone))

(defun canrylog-parse-iso8601 (thing)
  "Parse THING as an ISO 8601 timestamp into an encoded time value."
  (parse-iso8601-time-string thing))

(defun canrylog-time-coerce (thing)
  "Try to turn THING into an encoded time value."
  (cl-typecase thing
    ;; encoded time or nil
    (list thing)
    ;; unix time
    (number (time-convert thing 'list))
    (string
     (if (< (length thing) 20)
         ;; unix time in a string
         (seconds-to-time (string-to-number thing))
       ;; iso 8601
       (canrylog-parse-iso8601 thing)))
    (canrylog-event
     (canrylog-time-coerce (canrylog-event-moment thing)))))

(defun canrylog-time-coerce--unix (thing &rest type)
  "Try to turn THING into a unix timestamp.
If TYPE is `float', return a float (as from `float-time') instead."
  (cl-block nil
    ;; Special case early return
    ;; to avoid number -> encoded time -> number conversion
    (when (numberp thing)
      (if (eq type 'float)
          ;; `float' returns the argument itself if it is already a float
          (cl-return (float thing))
        (cl-return thing)))
    (let ((encoded (canrylog-time-coerce thing)))
      (cond ((eq type 'float) (float-time encoded))
            (t (time-convert encoded 'integer))))))

(defun canrylog-time-clamp (time &optional interval)
  "Clamp TIME to within INTERVAL.
If INTERVAL is nil, just return TIME.
TIME is converted to unix time, as a float."
  (unless (numberp time)
    (setq time (float-time time)))
  (if (and interval (canrylog-interval-p interval))
      (max (canrylog-time-coerce--unix (oref interval start))
           (min time (canrylog-time-coerce--unix (oref interval end))))
    time))

(defun canrylog-time->= (&rest args)
  "Does each arg in ARGS describe a later timestamp than the next one?"
  (not (apply #'canrylog-time-< args)))

(defun canrylog-time-< (a &rest rest)
  "Return non-nil if each argument (A and REST) is `time-less-p' over the next arg.

If only A is present, compare it with the current time."
  (let (b)
    ;; If there is only one argument, compare it with the current time.
    (unless rest
      (push (current-time) rest))
    (setq a (canrylog-time-coerce a))
    (setq b (canrylog-time-coerce (car rest)))
    (while (time-less-p a b)
      (setq a b)
      (setq rest (cdr rest))
      (setq b (canrylog-time-coerce (car rest))))
    ;; If we went through the entire argument list (rest = nil), that
    ;; means everything is in order, so we return (not rest).
    (not rest)))

(defun canrylog-time-humanize (seconds)
  "Return a human readable string for SECONDS."
  (format-seconds
   (cond
    ;; Count in days above 100 hours
    ((> seconds (* 100 60 60))
     "%x %D %z")
    ;; Count just minutes below an hour
    ((< seconds (* 1 60 60))
     "%x %M %z")
    ;; Count just hours in between
    (t
     "%x %H %z"))
   seconds))

(defun canrylog-display-day (timestamp)
  "Display TIMESTAMP as a date in a friendly way."
  (when (and (stringp timestamp)
             (= 10 (length timestamp)))
    (setq timestamp (concat timestamp "T00:00:00")))
  (let ((format "%A, %B %-d")
        (time (parse-iso8601-time-string timestamp)))
    (unless (equal (format-time-string "%Y")
                   (format-time-string "%Y" time))
      (setq format (concat format ", %Y")))
    (format-time-string format time)))

(defun canrylog-display-month (timestamp)
  "Display TIMESTAMP as a month in a friendly way."
  (format-time-string
   "%B %Y"
   (canrylog-time-coerce timestamp)))

(defun canrylog-format-time (timestamp)
  "Format TIMESTAMP with `canrylog-time-format'."
  (format-time-string
   canrylog-time-format
   (canrylog-time-coerce timestamp)))

(defun canrylog-format-datetime (timestamp)
  "Format TIMESTAMP according to `canrylog-datetime-format'."
  (format-time-string
   canrylog-datetime-format
   (canrylog-time-coerce timestamp)))

(defun canrylog-format-month (timestamp)
  "Display TIMESTAMP's month."
  (format-time-string "%Y-%m" (canrylog-time-coerce timestamp)))

(defun canrylog-format-date (timestamp)
  "Display TIMESTAMP's date."
  (format-time-string "%Y-%m-%d" (canrylog-time-coerce timestamp)))

(defun canrylog-format-week (interval)
  "Display INTERVAL as a week."
  (format "%s ~ %s"
          (canrylog-format-date
           (oref interval start))
          (canrylog-format-date
           (oref interval end))))

(defun canrylog-format-duration (duration)
  "Display DURATION in HH:MM:SS."
  (format-seconds "%.2h:%.2m:%.2s" duration))

(defun canrylog-display-duration (duration)
  "Format DURATION for display."
  (canrylog::face 'canrylog-duration
    (canrylog-format-duration duration)))

(defun canrylog-display-time (time)
  "Format TIME for display."
  (canrylog::face 'canrylog-time
    (canrylog-format-time time)))

(defun canrylog-display-datetime (time)
  "Format TIME for display as a datetime."
  (or (-some-> time
        canrylog-format-datetime)
      "now"))

;;;; Event

(defun canrylog-serialize--moment (moment)
  "Serialize a MOMENT."
  (if (stringp moment)
      moment
    (pcase canrylog-timestamp-format
      ('unix moment)
      ('iso8601 (canrylog-iso8601 moment))
      ;; Any other value: warn then proceed as if it's `unix'.
      (val
       (progn
         (canrylog::warn
          "`canrylog-timestamp-format' should be one of `unix' or `iso8601', but instead it is `%S'. Proceeding as if it is set to `unix'"
          val)
         moment)))))
(cl-defmethod canrylog-serialize ((event canrylog-event))
  "Convert EVENT to string."
  (format
   "%s  %s"
   (canrylog-serialize--moment (canrylog-event-moment event))
   (canrylog-event-task event)))

;;;;; Interval (= Period)
;; start is canrylog-time
;; end is nil or a time value

;; Mostly for being easier to read during debugging
(cl-defmethod canrylog-serialize ((interval canrylog-interval))
  "Serialize INTERVAL."
  (prin1-to-string (cons
                    (canrylog-format-datetime (oref interval start))
                    (canrylog-format-datetime (oref interval end)))))

(defun canrylog-interval-from-duration (dur)
  "Return interval from DUR seconds ago to now."
  (let ((now (current-time)))
    (canrylog-interval :start (time-subtract now dur)
                       :end now)))

(defun canrylog-interval-intersection (a b)
  "Return the intersection of A and B as a new interval."
  (let ((start
         ;; min and max seem flipped because we're using less-than
         (-min-by #'canrylog-time-<
                  (--map (oref it start) (list a b))))
        (end
         (-max-by #'canrylog-time-<
                  (--map (oref it end) (list a b)))))
    (when (canrylog-time-< start end)
      (canrylog-interval :start start :end end))))

(defun canrylog-intersecting? (a b)
  "Are A and B intersecting each other?

A and B may be events or intervals."
  ;; This feels a bit... brute force.
  (cond
   ((canrylog-event-p a)
    (canrylog-time-< (oref b start)
                     (canrylog-event-moment a)
                     (oref b end)))
   ((canrylog-event-p b)
    (canrylog-time-< (oref a start)
                     (canrylog-event-moment b)
                     (oref a end)))
   (t
    (or
     (canrylog-time-< (oref a start)
                      (oref b start)
                      (oref a end))
     (canrylog-time-< (oref b start)
                      (oref a start)
                      (oref b end))))))

(defvar canrylog-interval-duration--bound nil
  "Bounds for `canrylog-interval-duration'.

If the end of an interval is nil, `canrylog-interval-duration'
treats it as referring to the current time. Let-bind this
variable to use a different time.")

(cl-defun canrylog-interval-duration (interval &optional range)
  "Return duration of INTERVAL.
If RANGE, clamp to within RANGE."
  (unless (oref interval start)
    (error "Interval must have a start when calculating its duration"))
  (unless (oref interval end)
    (setq interval (canrylog-interval
                    :start (oref interval start)
                    :end (or canrylog-interval-duration--bound
                             (current-time)))))
  (time-to-seconds
   (time-subtract
    (-> (canrylog-time-coerce (oref interval end))
        (canrylog-time-clamp range))
    (-> (canrylog-time-coerce (oref interval start))
        (canrylog-time-clamp range)))))

;;;;; Goals
(cl-defstruct (canrylog-goal (:copier nil)
                             (:constructor canrylog-goal))
  id name target description)

;;;;; Task
(cl-defstruct (canrylog-task
               (:copier nil)
               (:constructor canrylog-task))
  name
  (intervals nil))

(defun canrylog-task-= (a b)
  "Are A and B describing the same task?"
  (apply #'string=
         (mapcar
          ;; same name = same task and should be merged
          #'canrylog::ensure-task-name
          (list a b))))

(defun canrylog-tasks-in-events (events)
  "Return intervals in EVENTS along with which task they are switching to.

Return value is ((<interval> . <task-name>) ...).

We assume EVENTS is sorted by `canrylog-time-<' already (earlier
first, later last)."
  (->> (--find-indices it events)
       (--map (let* ((lst (nthcdr it events))
                     (this (car lst))
                     (task (oref this task))
                     (next (cadr lst)))
                (when this
                  (setq this (oref this moment)))
                (when next
                  (setq next (oref next moment)))
                (cons task
                      (canrylog-interval
                       :start this
                       :end next))))))

(defun canrylog-task-intervals-in-events (task events)
  "Return intervals of TASK in EVENTS.

If TASK is a function, return intervals of all tasks that makes it
return non-nil.

We assume EVENTS is sorted by `canrylog-time-<' already (earlier
first, later last)."
  (->> (--find-indices
        (if (functionp task)
            (funcall task (oref it task))
          (equal task (oref it task)))
        events)
       (--map (let* ((lst (nthcdr it events))
                     (this (car lst))
                     (next (cadr lst)))
                (when this
                  (setq this (oref this moment)))
                (when next
                  (setq next (oref next moment)))
                (canrylog-interval
                 :start this
                 :end next)))))

;;;;; Task names

;; f.el inspired
(defun canrylog::task-name:base (task)
  "Return basename of TASK.

Development:Canrylog -> Canrylog
Development:Canrylog:Emacs -> Emacs"
  (replace-regexp-in-string "^.*:" "" task))
(defun canrylog::task-name:parent (name)
  "Return the parent of NAME.
Root names have a parent of an empty string."
  (s-join ":" (butlast (s-split ":" name))))
(defun canrylog::task-name:parent-of-p (a b)
  "Return t if task name A is parent of B."
  (when-let (it (canrylog::task-name:parent b))
    (equal a it)))
(defun canrylog::task-name:child-of-p (a b)
  "Return t if task name A is child of B."
  (when-let (it (canrylog::task-name:parent a))
    (equal it b)))
(defun canrylog::task-name:descendant-of-p (task maybe-ancestor)
  "Is TASK a descendant of task MAYBE-ANCESTOR?"
  (s-prefix? (concat maybe-ancestor ":")
             task))

(defun canrylog::task-name:ancestors (task-name)
  "Return ancestors of TASK-NAME.

Given \"a:b:c\", return (\"a\" \"a:b\")."
  (--map (substring task-name 0 (car it))
         (s-matched-positions-all ":" task-name)))
(defun canrylog::task-name:top-level (task-name)
  "Return the top level name of TASK-NAME."
  (or (car (canrylog::task-name:ancestors task-name))
      task-name))

(defun canrylog::ensure-task-name (task-or-name)
  "Return TASK-OR-NAME as a name."
  (if (canrylog-task-p task-or-name)
      (canrylog-task-name task-or-name)
    task-or-name))

;;;; Provide
(provide 'canrylog-types)

;;; canrylog-types.el ends here
