;;; canrylog-test.el --- unit tests for canrylog -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(when (require 'undercover nil t)
  (undercover "*.el"))

(require 'ert)
(require 'canrylog)
(require 'parse-time)

(defconst canrylog-test-here
  (or (and load-file-name
           (file-name-directory load-file-name))
      default-directory))
(defun canrylog-test-path (path)
  "Resolve PATH to be under the test directory."
  (expand-file-name path canrylog-test-here))
(defmacro canrylog-with-test-buffer (&rest body)
  "Set up canrylog buffer before running BODY."
  `(with-temp-buffer
     (insert (string-trim-left "
946681200  eating
946695600  sleep
946695660  sleep
946699200  programming"))
     ,@body))
(defmacro canrylog-with-test-file (file &rest body)
  "Run BODY with FILE being `canrylog-file'.
FILE is resolved using `canrylog-test-path' to be under the test directory."
  (declare (indent 1))
  `(cl-letf (((symbol-function 'canrylog-file)
              ,(lambda ()
                 (canrylog-test-path file)))
             ;; Don't overwrite the value in my editing instance
             (canrylog-cache nil))
     ,@body))

(ert-deftest canrylog-time-< ()
  ;; < works with just one argument (comparing with now)
  (should (canrylog-time-< "2000-01-01T00:00:00Z"))
  ;; nil stands for now
  (should (not (canrylog-time-< nil "2000-01-01T00:00:00Z")))
  (should (canrylog-time-< "2000-01-01T00:00:00Z" nil "9999-01-01T00:00:00Z"))
  (should (not (canrylog-time-< "9999-01-01T00:00:00Z" nil)))
  ;; < works with two arguments
  (should (canrylog-time-< "2000-01-01T00:00:00Z" "2000-05-01T00:00:00Z"))
  (should (not (canrylog-time-< "2000-05-01T00:00:00Z" "2000-01-01T00:00:00Z")))
  ;; < works with more than two arguments
  (should (canrylog-time-< "2000-01-01T00:00:00Z" "2000-05-01T00:00:00Z"
                           "2000-09-01T00:00:00Z" "2000-10-01T00:00:00Z"))
  ;; unsorted element at boundry
  (should (not (canrylog-time-< "2000-01-01T00:00:00Z" "2000-05-01T00:00:00Z"
                                "2000-12-01T00:00:00Z" "2000-10-01T00:00:00Z")))
  ;; in A and B
  (should (not (canrylog-time-< "2000-06-01T00:00:00Z" "2000-05-01T00:00:00Z"
                                "2000-09-01T00:00:00Z" "2000-10-01T00:00:00Z")))
  ;; in REST
  (should (not (canrylog-time-< "2000-02-01T00:00:00Z" "2000-05-01T00:00:00Z"
                                "2000-09-01T00:00:00Z" "2000-04-01T00:00:00Z"))))
(ert-deftest canrylog-time->= ()
  (should (canrylog-time->=
           "2000-10-01T00:00:00Z"
           "2000-09-01T00:00:00Z"
           ;; equal values
           "2000-05-01T00:00:00Z"
           "2000-05-01T00:00:00Z"
           "2000-01-01T00:00:00Z"))
  (should (not (canrylog-time->=
                "2000-09-01T00:00:00Z"
                "2000-10-01T00:00:00Z"))))
(ert-deftest canrylog--encoded-time-p ()
  (should
   (and
    (not (canrylog--encoded-time-p "2023-12-20T02:47:27+0900"))
    (canrylog--encoded-time-p 1703008071)
    (canrylog--encoded-time-p 1.5)
    (canrylog--encoded-time-p '(25985 55111 0 0))))
  (should
   (and
    (canrylog--encoded-time-p (canrylog-time-coerce (canrylog-iso8601)))
    (canrylog--encoded-time-p (canrylog-time-coerce--unix (current-time))))))
(ert-deftest canrylog-iso8601 ()
  ;; We can call it in ways that we expect
  (should
   (stringp (canrylog-iso8601)))
  (should
   (stringp (canrylog-iso8601 (current-time)))))

(ert-deftest canrylog::task-name:base ()
  (should (equal (canrylog::task-name:base "Task1")
                 "Task1"))
  (should (equal (canrylog::task-name:base "Task1:Child")
                 "Child")))
(ert-deftest canrylog::task-name:parent ()
  (should (equal (canrylog::task-name:parent "")
                 ""))
  (should (equal (canrylog::task-name:parent "abc")
                 ""))
  (should (equal (canrylog::task-name:parent "abc:def")
                 "abc")))
(ert-deftest canrylog::task-name:parent-of-p ()
  (should (not (canrylog::task-name:parent-of-p "Task1" "Task2")))
  (should (not (canrylog::task-name:parent-of-p "Task1" "Task10")))
  (should (not (canrylog::task-name:parent-of-p "Task1" "Task2:Child")))
  (should (not (canrylog::task-name:parent-of-p "Task1" "Task1:Abc:Child")))
  (should (canrylog::task-name:parent-of-p "Task1" "Task1:Child")))
(ert-deftest canrylog::task-name:child-of-p ()
  (should (not (canrylog::task-name:child-of-p "Task2" "Task1")))
  (should (not (canrylog::task-name:child-of-p "Task10" "Task1")))
  (should (not (canrylog::task-name:child-of-p "Task2:Child" "Task1")))
  (should (not (canrylog::task-name:child-of-p "Task1:Abc:Child" "Task1")))
  (should (canrylog::task-name:child-of-p "Task1:Child" "Task1")))
(ert-deftest canrylog::task-name:descendant-of-p ()
  (should (not (canrylog::task-name:descendant-of-p "Task1" "Task2")))
  (should (not (canrylog::task-name:descendant-of-p "Task1" "Task10")))
  (should (not (canrylog::task-name:descendant-of-p "Task2:Child" "Task1")))
  (should (canrylog::task-name:descendant-of-p "Task1:Child" "Task1"))
  (should (canrylog::task-name:descendant-of-p "Task1:Abc:Child" "Task1")))
(ert-deftest canrylog::task-name:ancestors ()
  (should (not (seq-difference (canrylog::task-name:ancestors "a:b:c")
                               '("a" "a:b")))))

(ert-deftest canrylog-format-duration ()
  (should (equal (canrylog-format-duration 75949)
                 "21:05:49")))

(ert-deftest canrylog-events ()
  ;; serializes correcly
  (should (equal (canrylog-serialize
                  (canrylog-event :moment "1970-01-01T00:00:00+0000" :task "task"))
                 "1970-01-01T00:00:00+0000  task"))

  ;; has a working less-than operator
  (let ((event-a (canrylog-event :moment "1970-01-01T00:00:00+0000" :task "task"))
        (event-b (canrylog-event :moment "1970-01-02T00:00:00+0000" :task "task2"))
        (event-c (canrylog-event :moment "1970-01-02T00:00:00+0000" :task "task2")))
    (should (canrylog-time-< event-a event-b))
    (should (not (canrylog-time-< event-b event-a)))
    (should (not (canrylog-time-< event-b event-c)))))

(ert-deftest canrylog-interval-intersection ()
  ;; Chopping off one end
  (should (equal
           (canrylog-interval-intersection
            (canrylog-interval :start "2021-05-22T13:37:10+0900"
                               :end "2021-05-23T02:55:08+0900")
            (canrylog-interval :start "2021-05-23T00:00:00+0900"
                               :end "2021-05-23T23:59:59+0900"))
           (canrylog-interval :start "2021-05-23T00:00:00+0900"
                              :end "2021-05-23T02:55:08+0900")))
  ;; A includes B
  (should (equal
           (canrylog-interval-intersection
            (canrylog-interval :start "2021-05-22T13:37:10+0900"
                               :end "2021-05-23T02:55:08+0900")
            (canrylog-interval :start "2021-05-22T14:00:00+0900"
                               :end "2021-05-23T01:00:00+0900"))
           (canrylog-interval :start "2021-05-22T14:00:00+0900"
                              :end "2021-05-23T01:00:00+0900")))
  ;; No intersection
  (should (null
           (canrylog-interval-intersection
            (canrylog-interval :start "2021-05-22T13:37:10+0900"
                               :end "2021-05-23T02:55:08+0900")
            (canrylog-interval :start "2021-05-20T00:00:00+0900"
                               :end "2021-05-20T01:00:00+0900")))))
(ert-deftest canrylog-interval-duration ()
  ;; calculates durations correctly
  (should (= (canrylog-interval-duration
              (canrylog-interval :start "2000-01-01T00:00:00+0900"
                                 :end "2000-01-02T00:00:00+0900"))
             86400))
  (should (= (canrylog-interval-duration
              (canrylog-interval :start "2000-01-01T00:00:00+0900"
                                 :end "2000-01-02T00:00:00+0900"))
             86400))
  (should (= (canrylog-interval-duration
              (canrylog-interval :start "2001-01-01T00:00:00+0900"
                                 :end "2000-12-31T23:00:00+0800"))
             0)))
(ert-deftest canrylog-interval-from-duration ()
  ;; constructs intervals properly
  (should (let* ((interval (canrylog-interval-from-duration 300)))
            (and (canrylog-interval-p interval)
                 (= (canrylog-interval-duration interval) 300)))))
(ert-deftest canrylog-interval-from-name ()
  (should (equal
           (let ((org-extend-today-until 0)
                 (calendar-week-start-day 0))
             (canrylog-interval-from-name 'week-containing "2023-02-27T00:01:02+0900"))
           (canrylog-interval
            :start (canrylog-time-coerce "2023-02-26T00:00:00+0900")
            :end (canrylog-time-coerce "2023-03-04T23:59:59+0900"))))
  (should (equal
           (let ((org-extend-today-until 1))
             (canrylog-interval-from-name 'day-containing "2023-02-27T00:01:02+0900"))
           (canrylog-interval
            :start (canrylog-time-coerce "2023-02-26T00:00:00+0900")
            :end (canrylog-time-coerce "2023-02-26T23:59:59+0900"))))
  ;; As I don't know of a way to control system time, I can only test
  ;; that it accepts these inputs and don't fail.
  (should (canrylog-interval-p (canrylog-interval-from-name 'day)))
  (should (canrylog-interval-p (canrylog-interval-from-name 'day -1)))
  (should (canrylog-interval-p (canrylog-interval-from-name 'week)))
  (should (canrylog-interval-p (canrylog-interval-from-name 'month))))

(ert-deftest canrylog-utils ()
  ;; returns duplicated items
  (should (equal (canrylog-duplicated '(a b c b a))
                 '(b a)))
  (should (equal (canrylog-keyword-to-symbol :keyword)
                 'keyword)))

(ert-deftest canrylog-parse-task-info ()
  (canrylog-with-test-file "iso8601.canrylog"
    (should
     (equal (canrylog-parse-task-info
             :info 'intervals
             :name "SocialMedia"
             :descendants t)
            '(#s(canrylog-interval 1702023960.0 1702031172.0)
              #s(canrylog-interval 1702021499.0 1702023960.0)
              #s(canrylog-interval 1702018708.0 1702021499.0))))
    (should
     (equal (canrylog-parse-task-info
             :info 'duration
             :name "SocialMedia"
             :descendants t)
            12464.0))
    (should
     (equal (canrylog-parse-task "Translation:KDE")
            #s(canrylog-task
               "Translation:KDE"
               (#s(canrylog-interval 1702031172.0 1702031933.0)))))
    (should
     (equal (canrylog-current-task-name)
            "Downtime")))
  (canrylog-with-test-file "unix.canrylog"
    (should
     (equal (canrylog-parse-task-info
             :info 'intervals
             :name "SocialMedia"
             :descendants t)
            '(#s(canrylog-interval 1702023960.0 1702031172.0)
              #s(canrylog-interval 1702021499.0 1702023960.0)
              #s(canrylog-interval 1702018708.0 1702021499.0))))
    (should
     (equal (canrylog-parse-task-info
             :info 'duration
             :name "SocialMedia"
             :descendants t)
            12464.0))
    (should
     (equal (canrylog-parse-task "Translation:KDE")
            #s(canrylog-task
               "Translation:KDE"
               (#s(canrylog-interval 1702031172.0 1702031933.0)))))
    (should
     (equal (canrylog-current-task-name)
            "Downtime"))))
(ert-deftest canrylog-task-last-active ()
  (canrylog-with-test-file "unix.canrylog"
    (should (equal
             (canrylog-time-coerce--unix
              (canrylog-task-last-active "SocialMedia:Facebook"))
             (canrylog-time-coerce--unix
              "2023-12-08T17:26:00+0900"))))
  (canrylog-with-test-file "iso8601.canrylog"
    (should (equal
             (canrylog-time-coerce--unix
              (canrylog-task-last-active "SocialMedia:Facebook"))
             (canrylog-time-coerce--unix
              "2023-12-08T17:26:00+0900")))))
(ert-deftest canrylog-parse-task-names ()
  (canrylog-with-test-file "iso8601.canrylog"
    (should
     (equal
      (let ((canrylog-sort-tasks-by-last-active t))
        (canrylog-parse-task-names))
      '("Downtime"
        "Translation:KDE"
        "Translation"
        "SocialMedia:Facebook"
        "SocialMedia:Youtube"
        "SocialMedia:Reddit"
        "SocialMedia")))
    (should
     (equal
      (let ((canrylog-sort-tasks-by-last-active nil))
        (canrylog-parse-task-names))
      '("SocialMedia:Reddit"
        "SocialMedia:Youtube"
        "SocialMedia:Facebook"
        "SocialMedia"
        "Translation:KDE"
        "Translation"
        "Downtime")))))

(ert-deftest canrylog-file-event-at-point ()
  ;; correctly inserted events
  (canrylog-with-test-buffer
   (goto-char (1- (point-max)))
   (should (equal (canrylog-file-event-at-point)
                  (canrylog-event :moment "946699200"
                                  :task "programming")))))
(ert-deftest canrylog-file-cleanup ()
  (canrylog-with-test-buffer
   (canrylog-file-cleanup (point-min) (point-max))
   (should (equal (string-trim (buffer-string))
                  (string-trim "
946681200  eating
946695600  sleep
946699200  programming")))))
(ert-deftest canrylog-file-do-merge ()
  (canrylog-with-test-file "merge-conflict.canrylog"
    (canrylog-with-file nil
      (canrylog-file-do-merge)
      (should (equal (string-trim-left (buffer-string))
                     (string-trim-left "
2019-03-15T13:14:00+0900  C
2019-03-15T14:30:00+0900  B
2019-03-15T14:53:00+0900  C
2019-03-15T16:12:00+0900  A
2019-03-16T16:10:00+0900  A
2019-03-16T16:30:00+0900  B
2019-03-16T18:25:00+0900  A
2019-03-16T19:35:00+0900  B
"))))))

(ert-deftest canrylog-db--flatten ()
  (should
   (equal (canrylog-db--flatten
           '(("a") ("b")))
          '("a" "b")))
  (should
   (equal (canrylog-db--flatten
           '(("a" "A") ("b" "B")))
          '("a" "b"))))

(ert-deftest canrylog-task ()
  (canrylog-with-test-file "unix.canrylog"
    (should (time-equal-p
             (canrylog-task-first-active
              '("SocialMedia:Youtube" "Translation:KDE"))
             (canrylog-time-coerce "2023-12-08T16:44:59+0900"))))
  (canrylog-with-test-file "unix.canrylog"
    (should
     (equal (canrylog-task-children "SocialMedia")
            '("SocialMedia:Facebook" "SocialMedia:Youtube" "SocialMedia:Reddit")))
    (should
     (equal (canrylog-task-children "Translation")
            '("Translation:KDE"))))
  (canrylog-with-test-file "unix.canrylog"
    (should (= (canrylog-task-duration "SocialMedia:Facebook")
               7212))
    (should (= (canrylog-task-duration "SocialMedia")
               0))
    (should (= (canrylog-task-duration-plus-children "SocialMedia")
               12464)))
  (canrylog-with-test-file "unix.canrylog"
    ;; Declared tasks exist
    (should (canrylog-task-exists? "Translation:KDE"))
    ;; Parent of declared tasks should also exist
    (should (canrylog-task-exists? "Translation"))
    (should (not (canrylog-task-exists? "ABC"))))
  (canrylog-with-test-file "unix.canrylog"
    ;; Inactive task = nil
    (should (not (canrylog-task-running "Translation:KDE")))
    ;; Active task duration should not be cached
    (should
     (< (canrylog-task-running "Downtime")
        (progn (sleep-for 0.01)
               (canrylog-task-running "Downtime"))))))

(ert-deftest canrylog-switch-task ()
  (canrylog-with-test-file "another.canrylog"
    (canrylog-switch-task "Task1" "2023-12-01T00:10:00+0900")
    ;; should not have actually switched
    (should (equal 1 (length (canrylog-get-events))))

    (canrylog-switch-task "Task2" "2023-12-01T00:10:00+0900")
    ;; should have switched
    (should (equal 2 (length (canrylog-get-events))))
    (should (equal "Task2" (canrylog-current-task-name))))
  ;; HACK: restore the file and reset the changes
  (call-process "git" nil nil nil "checkout" (canrylog-test-path "another.canrylog")))

;;; canrylog-test.el ends here
